package com.ptweb.production.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "factory")
public class Factory extends BaseEntity
{
	@Column(name = "name")
	private String name;

	@Column(name = "address")
	private String address;

	@Column(name = "branch_id")
	private Long branchId;

	@Column(name = "ware_house_id")
	private Long wareHouseId;
}
