package com.ptweb.production.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

//Lịch sử sản xuất
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "production_history")
public class ProductionHistory extends BaseEntity
{
	@Column(name = "factory_id")
	private Long factoryId;

	@Column(name = "category_id")
	private Long categoryId;

	@Column(name = "count")
	private Long count;
}
