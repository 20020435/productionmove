package com.ptweb.production.payload.request;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class CategoryRequest
{
	private long id;
	private String name;
	private String shortDescription;
	private String content;
	private MultipartFile image;
	private Long price;
	private Long warrantyPeriod;
	private String code;
	private String engineType;
	private String gear;
	private String cylinderCapacity;
	private String maximumOutput;
	private String maximumTorque;
	private String maxSpeed;
	private String tankCapacity;
	private String fuelPumpSystem;
}
