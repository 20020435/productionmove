package com.ptweb.production.payload.response;

import com.ptweb.production.entity.Category;
import com.ptweb.production.entity.ProductStatus;
import com.ptweb.production.util.MapperUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorRateResponse
{
	private CategoryResponse category;
	private double rate;

	public ErrorRateResponse(Category category, double rate)
	{
		this.category = MapperUtils.map(category, CategoryResponse.class);
		this.rate = rate;
	}

	public ErrorRateResponse(Category category, long errorCount, long totalCount)
	{
		this.category = MapperUtils.map(category, CategoryResponse.class);
		this.rate = 100 * ((double)errorCount / totalCount);
	}
}
