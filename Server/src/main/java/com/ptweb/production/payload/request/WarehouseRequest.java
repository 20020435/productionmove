package com.ptweb.production.payload.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WarehouseRequest
{
	private long id;
	private String name;
	private String address;
}
