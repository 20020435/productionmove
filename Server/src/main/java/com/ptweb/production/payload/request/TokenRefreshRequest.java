package com.ptweb.production.payload.request;

import javax.validation.constraints.NotNull;

public class TokenRefreshRequest
{
	private String refreshToken;

	public String getRefreshToken()
	{
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken)
	{
		this.refreshToken = refreshToken;
	}
}
