package com.ptweb.production.payload.request;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class PaymentRequest
{
	private Long warehouseId;
	private Long categoryId;
	private String customerName;
}
