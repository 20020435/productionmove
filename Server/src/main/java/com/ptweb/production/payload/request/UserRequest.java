package com.ptweb.production.payload.request;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UserRequest
{
	private Long id;
	private Date createdDate;
	private Date modifiedDate;
	private String createdBy;
	private String modifiedBy;
	private String username;
	private String password;
	private String fullName;
	private String phoneNumber;
	private String image;
	private String address;
	private String role;
	private Long branchId;
}
