package com.ptweb.production.payload.response;

import com.ptweb.production.entity.ProductStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatisticalResponse
{
	private String name;
	private long count;

	public StatisticalResponse(String name, long count)
	{
		this.name = name;
		this.count = count;
	}

	public StatisticalResponse(ProductStatus status, long count)
	{
		this.name = status.toString();
		this.count = count;
	}
}
