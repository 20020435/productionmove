package com.ptweb.production.payload.response;

import com.ptweb.production.util.PropertiesLoader;

import java.io.IOException;

public class TokenRefreshResponse
{
	private AccessToken accessToken;
	private RefreshToken refreshToken;
	private String tokenType = "Bearer";

	public TokenRefreshResponse(String accessToken, String refreshToken) throws IOException
	{
		this.accessToken = new AccessToken(accessToken);
		this.refreshToken = new RefreshToken(refreshToken);
	}

	public AccessToken getAccessToken()
	{
		return accessToken;
	}

	public void setAccessToken(AccessToken token)
	{
		this.accessToken = token;
	}

	public RefreshToken getRefreshToken()
	{
		return refreshToken;
	}

	public void setRefreshToken(RefreshToken refreshToken)
	{
		this.refreshToken = refreshToken;
	}

	public String getTokenType()
	{
		return tokenType;
	}

	public void setTokenType(String tokenType)
	{
		this.tokenType = tokenType;
	}

	private static class AccessToken
	{
		public String token;
		public Long expires;

		public AccessToken(String token) throws IOException
		{
			this.token = token;
			expires = Long.valueOf(PropertiesLoader.loadProperties("application.properties").
					getProperty("app.jwtExpirationMs"));
		}
	}

	private static class RefreshToken
	{
		public String token;
		public Long expires;

		public RefreshToken(String token) throws IOException
		{
			this.token = token;
			expires = Long.valueOf(PropertiesLoader.loadProperties("application.properties").
					getProperty("app.jwtRefreshExpirationMs"));
		}
	}
}
