package com.ptweb.production.payload.request;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
@Getter
@Setter
public class AddProductRequest
{
	private int categoryId;
	private int count;
}
