package com.ptweb.production.payload.response;

import com.ptweb.production.util.PropertiesLoader;

import java.io.IOException;

public class JwtResponse
{
	private AccessToken accessToken;
	private RefreshToken refreshToken;

	public JwtResponse(String accessToken, String refreshToken) throws IOException
	{
		this.accessToken = new AccessToken(accessToken);
		this.refreshToken = new RefreshToken(refreshToken);
	}

	public AccessToken getAccessToken()
	{
		return accessToken;
	}

	public void setAccessToken(AccessToken accessToken)
	{
		this.accessToken = accessToken;
	}

	public RefreshToken getRefreshToken()
	{
		return refreshToken;
	}

	public void setRefreshToken(RefreshToken refreshToken)
	{
		this.refreshToken = refreshToken;
	}

	private static class AccessToken
	{
		public String token;
		public Long expires;

		public AccessToken(String token) throws IOException
		{
			this.token = token;
			expires = System.currentTimeMillis() +
					Long.parseLong(PropertiesLoader.getProperty("app.jwtExpirationMs"));
		}
	}

	private static class RefreshToken
	{
		public String token;
		public Long expires;

		public RefreshToken(String token) throws IOException
		{
			this.token = token;
			expires = System.currentTimeMillis() +
					Long.parseLong(PropertiesLoader.getProperty("app.jwtRefreshExpirationMs"));
		}
	}
}
