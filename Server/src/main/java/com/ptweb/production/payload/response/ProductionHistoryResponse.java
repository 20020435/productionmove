package com.ptweb.production.payload.response;

import com.ptweb.production.entity.Category;
import com.ptweb.production.util.MapperUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ProductionHistoryResponse
{
	private CategoryResponse category;
	private long count;
	private Date created;

	public ProductionHistoryResponse(Category category, long count, Date created)
	{
		this.category = MapperUtils.map(category, CategoryResponse.class);
		this.count = count;
		this.created = created;
	}

	public ProductionHistoryResponse()
	{
	}
}
