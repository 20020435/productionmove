package com.ptweb.production.payload.request;

import com.ptweb.production.entity.Category;
import com.ptweb.production.entity.Product;
import com.ptweb.production.payload.response.CategoryResponse;
import com.ptweb.production.util.MapperUtils;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
@Getter
@Setter
public class ProductWarrantyRequest
{
	private long productId;
	private long serviceCenterId;
	private String productCode;
	private String errorMessage;
}
