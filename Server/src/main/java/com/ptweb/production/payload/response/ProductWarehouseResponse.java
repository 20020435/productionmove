package com.ptweb.production.payload.response;

import com.ptweb.production.entity.WareHouse;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
@Getter
@Setter
public class ProductWarehouseResponse
{
	private long warehouseId;
	private long count;
	private String warehouseName;
	private String warehouseAddress;

	public ProductWarehouseResponse()
	{
	}

	public ProductWarehouseResponse(long count, WareHouse wareHouse)
	{
		this.count = count;
		warehouseName = wareHouse.getName();
		warehouseAddress = wareHouse.getAddress();
		warehouseId = wareHouse.getId();
	}
}
