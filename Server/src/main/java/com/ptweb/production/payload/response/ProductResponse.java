package com.ptweb.production.payload.response;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
@Getter
@Setter
public class ProductResponse
{
	private int id;
	private String code;
	private int categoryId;
}
