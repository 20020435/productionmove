package com.ptweb.production.payload.response;

import com.ptweb.production.entity.Category;
import com.ptweb.production.util.MapperUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class DistributionHistoryResponseTwo
{
	private long id;
	private String factory;
	private CategoryResponse category;
	private long count;
	private Date created;

	public DistributionHistoryResponseTwo(long id, String factory, Category category,
										  long count, Date created)
	{
		this.id = id;
		this.factory = factory;
		this.category = MapperUtils.map(category, CategoryResponse.class);
		this.count = count;
		this.created = created;
	}

	public DistributionHistoryResponseTwo()
	{
	}
}
