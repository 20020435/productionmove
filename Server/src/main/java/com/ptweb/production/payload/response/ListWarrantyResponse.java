package com.ptweb.production.payload.response;

import com.ptweb.production.entity.Category;
import com.ptweb.production.entity.Product;
import com.ptweb.production.util.MapperUtils;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
@Getter
@Setter
public class ListWarrantyResponse
{
	private long id;
	private long productId;
	private String code;
	private CategoryResponse category;
	private String errorMessage;
	private String status;

	public ListWarrantyResponse(long id, Product product, Category category, String errorMessage, long status)
	{
		this.id = id;
		this.productId = product.getId();
		this.code = product.getCode();
		this.category = MapperUtils.map(category, CategoryResponse.class);
		this.errorMessage = errorMessage;
		if(status == 0)
			this.status = "Đang chờ bảo hành";
		if(status == 1)
			this.status = "Đang bảo hành";
	}

	public ListWarrantyResponse()
	{
	}
}
