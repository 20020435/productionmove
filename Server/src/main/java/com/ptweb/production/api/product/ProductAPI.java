package com.ptweb.production.api.product;

import com.ptweb.production.entity.ProductStatus;
import com.ptweb.production.payload.request.AddProductRequest;
import com.ptweb.production.payload.request.UserRequest;
import com.ptweb.production.payload.response.MessageResponse;
import com.ptweb.production.service.IProductService;
import com.ptweb.production.util.LoggerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@CrossOrigin
@RestController
@RequestMapping("/api/product")
public class ProductAPI
{
	@Autowired
	IProductService productService;

	//$2a$10$6kkfbQjMwGR1sVHt3t3krex9MwkmQzZk5J8Wri6Kji8jjtKwGAosK
	@Secured({"ROLE_FACTORY","ROLE_ADMIN"})
	@PostMapping
	public ResponseEntity<?> addProduct(@RequestBody AddProductRequest request)
	{
		try
		{
			return ResponseEntity.ok(productService.addProduct(request));
		}
		catch (Exception e)
		{
			LoggerUtils.log(e);
			return ResponseEntity.badRequest().body(new MessageResponse("Failed to add product!!!"));
		}
	}

	@GetMapping("/status/all")
	public ResponseEntity<?> getStatus()
	{
		ProductStatus[] statuses = ProductStatus.values();
		List<Map<String,Object>> list = new LinkedList<>();
		for(ProductStatus status : statuses)
		{
			Map<String,Object> map = new HashMap<>();
			map.put("name", status.toString());
			map.put("id", status.getStatus());
			list.add(map);
		}
		return ResponseEntity.ok(list);
	}
}