package com.ptweb.production.api.auth;

import com.ptweb.production.entity.RefreshToken;
import com.ptweb.production.entity.User;
import com.ptweb.production.exception.TokenRefreshException;
import com.ptweb.production.payload.request.AuthRequest;
import com.ptweb.production.payload.request.TokenRefreshRequest;
import com.ptweb.production.payload.response.JwtResponse;
import com.ptweb.production.payload.response.MessageResponse;
import com.ptweb.production.payload.response.TokenRefreshResponse;
import com.ptweb.production.service.IUserService;
import com.ptweb.production.service.impl.RefreshTokenService;
import com.ptweb.production.service.impl.UserDetailsImpl;
import com.ptweb.production.util.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@CrossOrigin
@RestController
@RequestMapping("/api/auth")
public class AuthApi
{
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	IUserService userService;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	RefreshTokenService refreshTokenService;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@RequestBody AuthRequest request) throws IOException
	{
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		String jwt = jwtUtils.generateJwtToken(userDetails);
		RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getId());
		return ResponseEntity.ok(new JwtResponse(jwt, refreshToken.getToken()));
	}

	@PostMapping("/refreshtoken")
	public ResponseEntity<?> refreshtoken(@RequestBody TokenRefreshRequest request)
	{
		String requestRefreshToken = request.getRefreshToken();

		return refreshTokenService.findByToken(requestRefreshToken)
				.map(refreshTokenService::verifyExpiration)
				.map(RefreshToken::getUser)
				.map(user -> {
					try
					{
						String token = jwtUtils.generateTokenFromUsername(user.getUserName());
						return ResponseEntity.ok(new TokenRefreshResponse(token, requestRefreshToken));
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
					return null;
				})
				.orElseThrow(() -> new TokenRefreshException(requestRefreshToken,
						"Refresh token is not in database!"));
	}

	@PostMapping("/signout")
	public ResponseEntity<?> logoutUser()
	{
		UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Long userId = userDetails.getId();
		refreshTokenService.deleteByUserId(userId);
		return ResponseEntity.ok(new MessageResponse("Log out successful!"));
	}

}