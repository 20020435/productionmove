package com.ptweb.production.api.branch;

import com.ptweb.production.payload.request.BranchRequest;
import com.ptweb.production.service.IBranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/branch")
@Secured("ROLE_ADMIN")
public class BranchAPI
{
	@Autowired
	private IBranchService iBranchService;

	@PostMapping
	public ResponseEntity<?> createBranch(@RequestBody BranchRequest request)
	{
		return ResponseEntity.ok(iBranchService.save(request));
	}

	@PutMapping
	public ResponseEntity<?> updateBranch(@RequestBody BranchRequest request)
	{
		return ResponseEntity.ok(iBranchService.update(request));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteBranch(@PathVariable("id") long id)
	{
		iBranchService.delete(id);
		return ResponseEntity.ok("ok");
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getBranch(@PathVariable("id") Long id)
	{
		return ResponseEntity.ok(iBranchService.findOneById(id));
	}

	@GetMapping("/all")
	public ResponseEntity<?> getAllBranch()
	{
		return ResponseEntity.ok(iBranchService.findAll());
	}

	@GetMapping("/{id}/user/all")
	public ResponseEntity<?> getAllBranch(@PathVariable("id") long branchId)
	{
		return ResponseEntity.ok(iBranchService.findAllUser(branchId));
	}
}