package com.ptweb.production.api.distributionAgent;

import com.ptweb.production.payload.request.DistributionStatisticalRequest;
import com.ptweb.production.payload.request.ServiceCenterStatisticalRequest;
import com.ptweb.production.service.IDistributionAgentService;
import com.ptweb.production.service.IServiceCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
@CrossOrigin
@RestController(value = "statisticalOfDistributionAgent")
@RequestMapping("/api/distribution-agent/statistical")
@Secured("ROLE_DISTRIBUTION_AGENT")
public class StatisticalAPI
{
	@Autowired
	IDistributionAgentService agentService;

	@PostMapping("/sell-product")
	public ResponseEntity<?> statisticalProductByStatus(@RequestBody DistributionStatisticalRequest request)
	{
		return ResponseEntity.ok(agentService.statisticalSellProduct(request));
	}
}
