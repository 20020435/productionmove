package com.ptweb.production.api.factory;

import com.ptweb.production.payload.request.FactoryStatisticalRequest;
import com.ptweb.production.service.IFactoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
@CrossOrigin
@RestController(value = "statisticalOfFactory")
@RequestMapping("/api/factory/statistical")
@Secured("ROLE_FACTORY")
public class StatisticalAPI
{
	@Autowired
	IFactoryService factoryService;

	@PostMapping("/status")
	public ResponseEntity<?> statisticalProductByStatus(@RequestBody FactoryStatisticalRequest request)
	{
		return ResponseEntity.ok(factoryService.statisticalProductByStatus(request));
	}

	@PostMapping("/sell-product")
	public ResponseEntity<?> statisticalSellProduct(@RequestBody FactoryStatisticalRequest request)
	{
		return ResponseEntity.ok(factoryService.statisticalSellProduct(request));
	}

	@PostMapping("/error-product")
	public ResponseEntity<?> statisticalErrorProduct(@RequestBody FactoryStatisticalRequest request)
	{
		return ResponseEntity.ok(factoryService.statisticalErrorProduct(request));
	}
}
