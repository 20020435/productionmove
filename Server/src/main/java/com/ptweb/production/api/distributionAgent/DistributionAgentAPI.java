package com.ptweb.production.api.distributionAgent;

import com.ptweb.production.payload.request.PaymentRequest;
import com.ptweb.production.payload.request.ProductWarrantyRequest;
import com.ptweb.production.payload.request.WarehouseRequest;
import com.ptweb.production.payload.response.MessageResponse;
import com.ptweb.production.service.IDistributionAgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
@CrossOrigin
@RestController
@RequestMapping("/api/distribution-agent")
public class DistributionAgentAPI
{
	@Autowired
	IDistributionAgentService agentService;

	@Secured({"ROLE_FACTORY", "ROLE_ADMIN", "ROLE_SERVICE_CENTER", "ROLE_DISTRIBUTION_AGENT"})
	@GetMapping("/all")
	public ResponseEntity<?> getAll()
	{
		return ResponseEntity.ok(agentService.getAll());
	}

	@Secured("ROLE_DISTRIBUTION_AGENT")
	@PostMapping("/warehouse")
	public ResponseEntity<?> addWarehouse(@RequestBody WarehouseRequest request)
	{
		return ResponseEntity.ok(agentService.addWarehouse(request));
	}

	@Secured("ROLE_DISTRIBUTION_AGENT")
	@PutMapping("/warehouse")
	public ResponseEntity<?> updateWarehouse(@RequestBody WarehouseRequest request)
	{
		return ResponseEntity.ok(agentService.updateWarehouse(request));
	}

	@Secured("ROLE_DISTRIBUTION_AGENT")
	@DeleteMapping("/warehouse/{id}")
	public ResponseEntity<?> deletWarehouse(@PathVariable("id") long id)
	{
		agentService.deleteWarehouse(id);
		return ResponseEntity.ok("ok");
	}

	@Secured("ROLE_DISTRIBUTION_AGENT")
	@GetMapping("/warehouse/all")
	public ResponseEntity<?> getAllWarehouse()
	{
		return ResponseEntity.ok(agentService.findAllWarehouse());
	}

	@Secured("ROLE_DISTRIBUTION_AGENT")
	@GetMapping("/order/all")
	public ResponseEntity<?> getAllOrder()
	{
		return ResponseEntity.ok(agentService.getAllOrder());
	}

	@Secured("ROLE_DISTRIBUTION_AGENT")
	@PutMapping("/order/{id}")
	public ResponseEntity<?> updateOrder(@PathVariable("id") long id,
										 @RequestParam("warehouseId") long warehouseId)
	{
		return ResponseEntity.ok(agentService.updateOrder(id, warehouseId));
	}

	@Secured("ROLE_DISTRIBUTION_AGENT")
	@PostMapping("/warranty")
	public ResponseEntity<?> addWarranty(@RequestBody ProductWarrantyRequest request)
	{
		String s = agentService.addWarranty(request);
		if (s.equalsIgnoreCase("Out of warranty period date"))
			return ResponseEntity.badRequest().body(new MessageResponse("Failed to add warranty!!!"));
		return ResponseEntity.ok(s);
	}

	@Secured("ROLE_DISTRIBUTION_AGENT")
	@GetMapping("/product/count")
	public ResponseEntity<?> addWarranty(@RequestParam("categoryId") long id)
	{
		return ResponseEntity.ok(agentService.countProduct(id));
	}

	@Secured("ROLE_DISTRIBUTION_AGENT")
	@PostMapping("/payment")
	public ResponseEntity<?> addPayment(@RequestBody PaymentRequest request)
	{
		return ResponseEntity.ok(agentService.addPayment(request));
	}

	@Secured("ROLE_DISTRIBUTION_AGENT")
	@GetMapping("/warranty/error/all")
	public ResponseEntity<?> getWarrantyError()
	{
		return ResponseEntity.ok(agentService.getWarrantyError());
	}

	@Secured("ROLE_DISTRIBUTION_AGENT")
	@GetMapping("/warranty/done/all")
	public ResponseEntity<?> getWarrantyDone()
	{
		return ResponseEntity.ok(agentService.getWarrantyDone());
	}

	@Secured("ROLE_DISTRIBUTION_AGENT")
	@PutMapping("/warranty/done/{id}")
	public ResponseEntity<?> updateWarrantyDone(@PathVariable("id") long id)
	{
		return ResponseEntity.ok(agentService.updateWarrantyDone(id));
	}

	@Secured("ROLE_DISTRIBUTION_AGENT")
	@PutMapping("/warranty/error/{id}")
	public ResponseEntity<?> updateWarrantyError(@PathVariable("id") long id)
	{
		return ResponseEntity.ok(agentService.updateWarrantyError(id));
	}

	@Secured("ROLE_DISTRIBUTION_AGENT")
	@GetMapping("/warranty/all")
	public ResponseEntity<?> getAllWarranty()
	{
		return ResponseEntity.ok(agentService.getAllWarranty());
	}
}
