package com.ptweb.production.model;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
public class CustomPageRequest extends PageRequest
{
	public CustomPageRequest(int page, int size, Sort sort)
	{
		super(page, size, sort);
	}

	public CustomPageRequest(int page, int size)
	{
		super(page, size, Sort.unsorted());
	}
}
