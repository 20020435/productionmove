package com.ptweb.production.repository;

import com.ptweb.production.entity.WarrantyHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WarrantyHistoryRepository extends JpaRepository<WarrantyHistory, Long>
{
	WarrantyHistory findOneById(long id);
}