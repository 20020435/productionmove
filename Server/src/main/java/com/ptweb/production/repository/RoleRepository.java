package com.ptweb.production.repository;

import com.ptweb.production.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>
{
	Role findOneByCodeContainingIgnoreCase(String code);
}