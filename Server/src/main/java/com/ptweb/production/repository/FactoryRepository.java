package com.ptweb.production.repository;

import com.ptweb.production.entity.Category;
import com.ptweb.production.entity.Factory;
import com.ptweb.production.entity.Product;
import com.ptweb.production.entity.ProductStatus;
import com.ptweb.production.payload.response.*;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface FactoryRepository extends JpaRepository<Factory, Long>
{
	@Query(value = "select ware_house_id from factory where branch_id = ?1 LIMIT 1", nativeQuery = true)
	long getWareHouseIdByBranchId(long branchId);

	@Query(value = "select id from factory where branch_id = ?1 LIMIT 1", nativeQuery = true)
	long getFactoryIdByBranchId(long branchId);

	@Query(value = "select p from Product p where p.factoryId = ?1 and p.productStatus = ?2 and p.category = ?3 ")
	List<Product> getProductToExportByCount(long factoryId, ProductStatus status,
											Category category, Pageable pageable);

	@Query("select new com.ptweb.production.payload.response.CountProductResponse(p.category, count(p)) from Product p where p.factoryId = ?1 " +
			"and p.productStatus = com.ptweb.production.entity.ProductStatus.NEWLY_PRODUCED  group by p.category")
	List<CountProductResponse> getCountProductByCategory(long factoryId);

	@Query("select new com.ptweb.production.payload.response.ProductionHistoryResponse" +
			"((select c from Category c where c.id = p.categoryId), p.count, p.createdDate)" +
			" from ProductionHistory p where p.factoryId = ?1")
	List<ProductionHistoryResponse> getProductionHistory(long factory);

	@Query("select new com.ptweb.production.payload.response.DistributionHistoryResponse" +
			"((select da.name from DistributionAgent da where da.id = d.distributionAgentId)," +
			"(select c from Category c where c.id = d.categoryId), d.count, d.createdDate)" +
			" from DistributionHistory d where d.factoryId = ?1")
	List<DistributionHistoryResponse> getDistributionHistory(long factory);

	@Query("select new com.ptweb.production.payload.response.CountProductResponse(p.category, count(p)) " +
			"from Product p where p.factoryId = ?1 and p.productStatus in ?2 and p.createdDate between ?3 and ?4  group by p.category")
	List<CountProductResponse> statisticalProductByStatus(Long factoryId, List<ProductStatus> ids,
														  Date from, Date to);

	@Query("select new com.ptweb.production.payload.response.CountProductResponse" +
			"((select c from Category c where c.id = d.categoryId), sum(d.count)) " +
			"from DistributionHistory d where d.factoryId = ?1 and d.createdDate between ?2 and ?3 group by d.categoryId")
	List<CountProductResponse> statisticalSellProduct(Long factoryId, Date from, Date to);

	@Query("select new com.ptweb.production.payload.response.ErrorRateResponse" +
			"((select c from Category c where c.id = p.category.id), " +
			"sum(case when p.warrantyCount > 0 then 1 else 0 end ) , count(p)) " +
			"from Product p where p.factoryId = ?1 and p.createdDate between ?2 and ?3 group by p.category")
	List<ErrorRateResponse> statisticalErrorProduct(Long factoryId, Date from, Date to);

	@Query("select new com.ptweb.production.payload.response.ProductWarrantyResponse" +
			"(w.id, (select p from Product p where p.id = w.productId), " +
			"(select p.category from Product p where p.id = w.productId) , w.errorMessage) " +
			"from WarrantyHistory w where w.factoryId = ?1 and w.status = 3")
	List<ProductWarrantyResponse> getErrorProduct(Long factoryId);
}