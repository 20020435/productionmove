package com.ptweb.production.repository;

import com.ptweb.production.entity.Product;
import com.ptweb.production.payload.response.StatisticalResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    Product findOneById(Long id);

    Product findOneByCodeIgnoreCase(String code);

    List<Product> findByCategoryId(Long id, Pageable pageable);

    @Query(value = "SELECT count(*) FROM product",nativeQuery = true)
    long countProducts();

    @Query(value = "SELECT count(*) FROM product where category_id = ?1",nativeQuery = true)
    long countProductsByCategoryId(long categoryId);

    @Query("SELECT new com.ptweb.production.payload.response.StatisticalResponse" +
            "(p.productStatus, count(p))" +
            " FROM Product p where p.category.id in ?1" +
            " and p.createdDate between ?2 and ?3 group by p.productStatus")
    List<StatisticalResponse> statisticalProductStatus(Long[] categoryId, Date from, Date to);

    @Query("SELECT new com.ptweb.production.payload.response.StatisticalResponse" +
            "((select f.name from Factory f where f.id = p.factoryId), count(p))" +
            " FROM ProductionHistory p where p.categoryId in ?1 " +
            " and p.createdDate between ?2 and ?3 group by p.factoryId")
    List<StatisticalResponse> statisticalProductFactory(Long[] categoryId, Date from, Date to);

    @Query("SELECT new com.ptweb.production.payload.response.StatisticalResponse" +
            "((select d.name from DistributionAgent d where d.id = t.distributionAgentId), count(t))" +
            " FROM TransactionHistory t where (select p.category.id from Product p" +
            " where p.id = t.productId) in ?1 and t.createdDate between ?2 and ?3 group by t.distributionAgentId")
    List<StatisticalResponse> statisticalProductTransaction(Long[] categoryId, Date from, Date to);

    @Query("SELECT new com.ptweb.production.payload.response.StatisticalResponse" +
            "((select s.name from ServiceCenter s where s.id = w.serviceCenterId), count(w))" +
            " FROM WarrantyHistory w where (select p.category.id from Product p" +
            " where p.id = w.productId) in ?1 and w.createdDate between ?2 and ?3 group by w.serviceCenterId")
    List<StatisticalResponse> statisticalProductWarranty(Long[] categoryId, Date from, Date to);
}