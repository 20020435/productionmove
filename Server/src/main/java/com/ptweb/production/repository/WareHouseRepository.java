package com.ptweb.production.repository;

import com.ptweb.production.entity.WareHouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface WareHouseRepository extends JpaRepository<WareHouse, Long>
{
	WareHouse findOneById(long id);

	@Query("select w from WareHouse w where w.id in " +
			"(select aw.warehouseId from AgentWareHouse aw where aw.distributionId = ?1)")
	List<WareHouse> findAllWithDistributionAgentId(Long agentId);
}