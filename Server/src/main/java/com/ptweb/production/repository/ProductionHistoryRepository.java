package com.ptweb.production.repository;

import com.ptweb.production.entity.ProductionHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductionHistoryRepository extends JpaRepository<ProductionHistory, Long>
{
}