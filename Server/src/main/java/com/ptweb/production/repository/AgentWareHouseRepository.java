package com.ptweb.production.repository;

import com.ptweb.production.entity.AgentWareHouse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AgentWareHouseRepository extends JpaRepository<AgentWareHouse, Long>
{
}