package com.ptweb.production.repository;

import com.ptweb.production.entity.DistributionHistory;
import com.ptweb.production.payload.response.DistributionHistoryResponse;
import com.ptweb.production.payload.response.DistributionHistoryResponseTwo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DistributionHistoryRepository extends JpaRepository<DistributionHistory, Long>
{
	@Query("select new com.ptweb.production.payload.response.DistributionHistoryResponseTwo" +
			"(d.id,(select f.name from Factory f where f.id = d.factoryId)," +
			"(select c from Category c where c.id = d.categoryId)," +
			"d.count, d.createdDate) " +
			"from DistributionHistory d where d.distributionAgentId = ?1 and d.status = 0")
	List<DistributionHistoryResponseTwo> findAllByDistributionId(Long distributionId);

	DistributionHistory findOneById(long id);
}