package com.ptweb.production.util;

import com.ptweb.production.service.impl.GoogleDiverService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
public class FileUploadUtils
{
	public static String uploadFileToGG(GoogleDiverService googleDiverService, MultipartFile multipartFile) throws IOException
	{
		Path path = Paths.get("Files-Upload");
		if (!Files.exists(path))
			Files.createDirectories(path);
		String fileCode = RandomStringUtils.randomAlphanumeric(8);
		try (InputStream inputStream = multipartFile.getInputStream())
		{
			String name = fileCode + "-" + multipartFile.getName();
			Path filePath = path.resolve(name);
			Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
			String url = googleDiverService.addGoogleDriveFiles(filePath.toFile(), name);
			filePath.toFile().delete();
			return url;
		}
		catch (IOException e)
		{
			throw new IOException("Could not save file:" + multipartFile.getName(), e);
		}
	}
}
