package com.ptweb.production.config;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.ptweb.production.util.PropertiesLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */

@Configuration
public class GoogleDriverConfig
{
	@Autowired
	GoogleCredential credential;
	@Bean
	public Drive getService() throws GeneralSecurityException, IOException {
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		return new Drive.Builder(HTTP_TRANSPORT,
				JacksonFactory.getDefaultInstance(), credential)
				.build();
	}

	@Bean
	public GoogleCredential googleCredential() throws GeneralSecurityException, IOException {
		Collection<String> elenco = new ArrayList<String>();
		elenco.add("https://www.googleapis.com/auth/drive");
		HttpTransport httpTransport = new NetHttpTransport();
		JacksonFactory jsonFactory = new JacksonFactory();
		Properties properties = PropertiesLoader.loadProperties();
		return new GoogleCredential.Builder().setTransport(httpTransport)
				.setJsonFactory(jsonFactory).setServiceAccountScopes(elenco)
				.setServiceAccountPrivateKeyFromP12File(ResourceUtils.getFile("classpath:bakery-key.p12"))
				.setClientSecrets(properties.getProperty("ggdriver.api.clientId"),"ggdriver.api.clientSecret")
				.setServiceAccountId(properties.getProperty("ggdriver.api.serviceAccountId"))
				.build()
				.setAccessToken(properties.getProperty("ggdriver.api.accessToken"))
				.setRefreshToken(properties.getProperty("ggdriver.api.refreshToken"));
	}
}
