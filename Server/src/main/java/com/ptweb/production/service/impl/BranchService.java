package com.ptweb.production.service.impl;

import com.ptweb.production.entity.*;
import com.ptweb.production.payload.request.BranchRequest;
import com.ptweb.production.payload.response.BranchResponse;
import com.ptweb.production.payload.response.UserResponse;
import com.ptweb.production.repository.*;
import com.ptweb.production.service.IBranchService;
import com.ptweb.production.util.MapperUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class BranchService implements IBranchService
{
	@Autowired
	private BranchRepository branchRepository;
	@Autowired
	private FactoryRepository factoryRepository;
	@Autowired
	private DistributionAgentRepository agentRepository;
	@Autowired
	private ServiceCenterRepository centerRepository;
	@Autowired
	private WareHouseRepository wareHouseRepository;
	@Autowired
	private UserRepository userRepository;

	@Override
	public BranchResponse findOneById(Long id)
	{
		return MapperUtils.map(branchRepository.findOneById(id), BranchResponse.class);
	}

	@Override
	public BranchResponse save(BranchRequest request)
	{
		Branch branch = new Branch();
		branch.setRole(request.getRole());
		branch.setName(request.getName());
		branch.setAddress(request.getAddress());
		branch = branchRepository.save(branch);
		String role = request.getRole();
		String address = request.getAddress();
		String name = request.getName();
		if (role.equalsIgnoreCase("factory"))
		{
			WareHouse wareHouse = new WareHouse();
			wareHouse.setName(name);
			wareHouse.setAddress(address);
			wareHouse = wareHouseRepository.save(wareHouse);
			Factory factory = new Factory();
			factory.setAddress(address);
			factory.setName(name);
			factory.setBranchId(branch.getId());
			factory.setWareHouseId(wareHouse.getId());
			factoryRepository.save(factory);
		}
		else if (role.equalsIgnoreCase("distribution_agent") ||
				role.equalsIgnoreCase("distribution-agent"))
		{
			DistributionAgent agent = new DistributionAgent();
			agent.setAddress(address);
			agent.setName(name);
			agent.setBranchId(branch.getId());
			agentRepository.save(agent);
		}
		else if (role.equalsIgnoreCase("service_center") ||
				role.equalsIgnoreCase("service-center"))
		{
			ServiceCenter serviceCenter = new ServiceCenter();
			serviceCenter.setAddress(address);
			serviceCenter.setName(name);
			serviceCenter.setBranchId(branch.getId());
			centerRepository.save(serviceCenter);
		}
		return MapperUtils.map(branch, BranchResponse.class);
	}

	@Override
	public List<BranchResponse> findAll()
	{
		List<Branch> list = branchRepository.findAll();
		return MapperUtils.mapList(list, BranchResponse.class);
	}

	@Override
	public List<UserResponse> findAllUser(long branchId)
	{
		List<UserResponse> list = new LinkedList<>();
		for(User user : userRepository.findAllByBranchId(branchId))
		{
			UserResponse response = MapperUtils.map(user, UserResponse.class);
			response.setRole(user.getRole().getCode());
			list.add(response);
		}
		return list;
	}

	@Override
	public BranchResponse update(BranchRequest request)
	{
		Branch branch = new Branch();
		branch.setId(request.getId());
		branch.setRole(request.getRole());
		branch.setName(request.getName());
		branch.setAddress(request.getAddress());
		branch = branchRepository.save(branch);
		return MapperUtils.map(branch, BranchResponse.class);
	}

	@Override
	public void delete(long id)
	{
		branchRepository.deleteById(id);
	}
}
