package com.ptweb.production.service;

import com.ptweb.production.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
public class Test
{
	@PersistenceContext
	private EntityManager entityManager;

	public void test()
	{
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
		Root<User> UserRoot = criteriaQuery.from(User.class);

		Predicate predicateForBlueColor
				= criteriaBuilder.equal(UserRoot.get("color"), "red");
		Predicate predicateForGradeA
				= criteriaBuilder.equal(UserRoot.get("grade"), "D");
		Predicate predicateForBlueColorAndGradeA
				= criteriaBuilder.and(predicateForBlueColor, predicateForGradeA);

		Predicate predicateForRedColor
				= criteriaBuilder.equal(UserRoot.get("color"), "blue");
		Predicate predicateForGradeB
				= criteriaBuilder.equal(UserRoot.get("grade"), "B");
		Predicate predicateForRedColorAndGradeB
				= criteriaBuilder.and(predicateForRedColor, predicateForGradeB);

		Predicate finalPredicate
				= criteriaBuilder
				.or(predicateForBlueColorAndGradeA, predicateForRedColorAndGradeB);
		criteriaQuery.where(finalPredicate);
		List<User> Users = entityManager.createQuery(criteriaQuery).getResultList();
	}
}
