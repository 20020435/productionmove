package com.ptweb.production.service;

import com.ptweb.production.payload.request.CategoryRequest;
import com.ptweb.production.payload.response.CategoryResponse;

import java.io.IOException;
import java.util.List;

public interface ICategoryService
{
	CategoryResponse findOneById(Long id);

	CategoryResponse findOneByCode(String id);

	CategoryResponse save(CategoryRequest request) throws Exception;

	List<CategoryResponse> findAll();

	CategoryResponse update(CategoryRequest request) throws Exception;

	void delete(long id);
}
