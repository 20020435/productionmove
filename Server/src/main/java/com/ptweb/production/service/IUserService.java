package com.ptweb.production.service;

import com.ptweb.production.entity.User;
import com.ptweb.production.payload.request.AuthRequest;
import com.ptweb.production.payload.request.UserRequest;
import com.ptweb.production.payload.response.UserResponse;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IUserService {
    UserResponse save(UserRequest Entity);

    UserResponse update(UserRequest Entity);

    UserResponse findOneById(Long id);

    UserResponse findOneByUserName(String name);

    long countUser();

    List<UserResponse> findAll(Pageable pageable);

    void deleteUserByIds(long[] ids);

    void delete(long id);
}
