package com.ptweb.production.service;

import com.ptweb.production.entity.RefreshToken;

import java.util.Optional;

/**
 * <p>Title: </p>
 * <p>Copyright (c) 2022</p>
 * <p>Company: </p>
 *
 * @author Nguyen Van Linh
 * @version 1.0
 */
public interface IRefreshTokenService
{
	Optional<RefreshToken> findByToken(String token);

	RefreshToken createRefreshToken(Long userId);

	RefreshToken verifyExpiration(RefreshToken token);

	int deleteByUserId(Long userId);
}
