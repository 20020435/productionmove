package com.ptweb.production.service;

import com.ptweb.production.payload.request.ExportProductRequest;
import com.ptweb.production.payload.request.FactoryStatisticalRequest;
import com.ptweb.production.payload.response.*;

import java.util.List;

public interface IFactoryService
{
	String exportProduct(ExportProductRequest request);

	List<CountProductResponse> getCountProduct();

	List<CountProductResponse> statisticalProductByStatus(FactoryStatisticalRequest request);

	List<CountProductResponse> statisticalSellProduct(FactoryStatisticalRequest request);

	List<ErrorRateResponse> statisticalErrorProduct(FactoryStatisticalRequest request);

	List<ProductionHistoryResponse> getProductionHistory();

	List<DistributionHistoryResponse> getDistributionHistory();

	List<ProductWarrantyResponse> getErrorProduct();

	String updateErrorProduct(long id);
}
