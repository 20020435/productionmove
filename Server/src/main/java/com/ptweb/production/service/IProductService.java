package com.ptweb.production.service;

import com.ptweb.production.entity.Product;
import com.ptweb.production.payload.request.AddProductRequest;
import com.ptweb.production.payload.response.ProductResponse;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IProductService
{
	List<ProductResponse> findAll(Pageable pageable);

	List<ProductResponse> findAllByCategory(long categoryId, Pageable pageable);

	void deleteItemsByIds(long[] ids);

	ProductResponse findOneById(Long id);

	void updateProduct(Product Entity);

	long countProducts();

	List<ProductResponse> addProduct(AddProductRequest request);
}
