package com.ptweb.production;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class ProductionMove
{
	public static void main(String[] args) throws IOException
	{
		SpringApplication.run(ProductionMove.class, args);
	}

}
