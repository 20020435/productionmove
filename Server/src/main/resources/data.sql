CREATE TABLE IF NOT EXISTS `role` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
CREATE TABLE IF NOT EXISTS `refreshtoken` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `createdby` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `createddate` datetime DEFAULT NULL,
    `modifiedby` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `modifieddate` datetime DEFAULT NULL,
    `expiry_date` datetime DEFAULT NULL,
    `token` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `UK_or156wbneyk8noo4jstv55ii3` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
CREATE TABLE IF NOT EXISTS `user` (
    `id` bigint NOT NULL AUTO_INCREMENT,
    `createdby` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `createddate` datetime DEFAULT NULL,
    `modifiedby` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `modifieddate` datetime DEFAULT NULL,
    `address` text COLLATE utf8_bin,
    `fullname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `image` text COLLATE utf8_bin,
    `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `phonenumber` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
    `refresh_token_id` bigint NOT NULL,
    `role_id` bigint DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FKo3wsa4hvodpbgd6upolukg7nj` (`refresh_token_id`),
    KEY `FKn82ha3ccdebhokx3a8fgdqeyy` (`role_id`),
    CONSTRAINT `FKn82ha3ccdebhokx3a8fgdqeyy` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
    CONSTRAINT `FKo3wsa4hvodpbgd6upolukg7nj` FOREIGN KEY (`refresh_token_id`) REFERENCES `refreshtoken` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
INSERT INTO role (id, code, name) SELECT * FROM (SELECT '1' as id, 'ADMIN' as code, 'ADMIN' as name) AS tmp WHERE NOT EXISTS (SELECT code FROM role WHERE code = 'ADMIN') LIMIT 1;
INSERT INTO role (id, code, name) SELECT * FROM (SELECT '2' as id, 'DISTRIBUTION_AGENT' as code, 'DISTRIBUTION AGENT' as name) AS tmp WHERE NOT EXISTS (SELECT code FROM role WHERE code = 'DISTRIBUTION_AGENT') LIMIT 1;
INSERT INTO role (id, code, name) SELECT * FROM (SELECT '3' as id, 'FACTORY' as code, 'FACTORY' as name) AS tmp WHERE NOT EXISTS (SELECT code FROM role WHERE code = 'FACTORY') LIMIT 1;
INSERT INTO role (id, code, name) SELECT * FROM (SELECT '4' as id, 'SERVICE_CENTER' as code, 'SERVICE CENTER' as name) AS tmp WHERE NOT EXISTS (SELECT code FROM role WHERE code = 'SERVICE_CENTER') LIMIT 1;
INSERT INTO refreshtoken (id) SELECT * FROM (SELECT '1' as id) AS tmp WHERE NOT EXISTS (SELECT id FROM refreshtoken WHERE id = '1') LIMIT 1;
Delete from user where id = '1';
INSERT INTO `user` (`id`, `fullname`, `password`, `username`, `refresh_token_id`, `role_id`) VALUES ('1', 'ADMIN', '$2a$10$fXPNGnniIlAtoBpGQGZBbuCScdj4Pdnj.BLd2lujHr0cYo2z7epzy', 'admin', '1', '1');