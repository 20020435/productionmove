-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: production_move
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agent_ware_house`
--

DROP TABLE IF EXISTS `agent_ware_house`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `agent_ware_house` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `distribution_id` bigint DEFAULT NULL,
  `warehouse_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent_ware_house`
--

LOCK TABLES `agent_ware_house` WRITE;
/*!40000 ALTER TABLE `agent_ware_house` DISABLE KEYS */;
INSERT INTO `agent_ware_house` VALUES (1,'agent','2022-12-30','agent','2022-12-30',1,2);
/*!40000 ALTER TABLE `agent_ware_house` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `branch`
--

DROP TABLE IF EXISTS `branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `branch` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `address` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `role` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branch`
--

LOCK TABLES `branch` WRITE;
/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
INSERT INTO `branch` VALUES (1,'Cầu giấy','Trung tâm bảo hành Cầu Giấy','service-center'),(2,'Hòa Lạc','Cơ sở sản xuất Cầu Giấy','factory'),(3,'Cầu giấy','Đại lý phân phối Cầu Giấy','distribution-agent');
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `content` text CHARACTER SET utf8mb3 COLLATE utf8mb3_bin,
  `cylinder_capacity` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `engine_type` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `fuel_pump_system` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `gear` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `image` text CHARACTER SET utf8mb3 COLLATE utf8mb3_bin,
  `max_speed` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `maximum_output` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `maximum_torque` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `price` bigint DEFAULT NULL,
  `short_description` text CHARACTER SET utf8mb3 COLLATE utf8mb3_bin,
  `tank_capacity` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `warranty_period` bigint DEFAULT '24',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'admin','2022-12-30','admin','2022-12-30','HCTR','Honda Civic Type R là phiên bản thể thao của mẫu xe Civic được sản xuất bởi hãng xe Honda - Nhật Bản. Lần đầu tiên được ra mắt công chúng vào năm 1997, được phát triển trên thế hệ thứ 6 của mẫu xe Civic.\r\nThế hệ thứ 5 của Honda Civic Type R từng được trưng bày tại Triển lãm ô tô VMS 2018 nhưng chưa được mở bán. Phải đến VMS 2022 diễn ra từ 26-10-2022 đến 30-10-2022, Honda Civic Type R mới chính thức được phân phối chính hãng.','1.996','2.0L DOHC VTEC Turbo, 4 xi lanh thẳng hàng, 16 van','PGM-FI','6MT','https://drive.google.com/uc?export=view&id=1OqODOcRoHAFIvAXdgW-JFztfui9xyh_F','272',NULL,'420/2.600 - 4.000','Honda Civic Type R',539000000,'Honda Civic Type R là phiên bản thể thao của mẫu xe Civic được sản xuất bởi hãng xe Honda - Nhật Bản. Lần đầu tiên được ra mắt công chúng vào năm 1997, được phát triển trên thế hệ thứ 6 của mẫu xe Civic.\r\nThế hệ thứ 5 của Honda Civic Type R từng được trưng bày tại Triển lãm ô tô VMS 2018 nhưng chưa được mở bán. Phải đến VMS 2022 diễn ra từ 26-10-2022 đến 30-10-2022, Honda Civic Type R mới chính thức được phân phối chính hãng.','47',33),(2,'admin','2022-12-30','admin','2022-12-30','HC','Honda City trong lần trở lại mới nhất đã có nhiều thay đổi đáng kể từ ngoại hình cho đến nội thất bên trong. Vì thế khiến cho cuộc đua doanh số trong phân khúc Sedan hạng B càng trở nên hấp dẫn hơn bao giờ hết với sự góp mặt của Toyota Vios, Hyundai Accent, Mazda 2, Kia Soluto.','1.498','1.5L DOHC i-VTEC 4 xi lanh thẳng hàng, 16 van','Phun xăng điện tử / PGM-FI','Vô cấp CVT','https://drive.google.com/uc?export=view&id=1F3jwla1_JAbN4ZDYHIY6XmyPpl1g32L4','230',NULL,'145/4.300','Honda City',529000000,'Honda City trong lần trở lại mới nhất đã có nhiều thay đổi đáng kể từ ngoại hình cho đến nội thất bên trong. Vì thế khiến cho cuộc đua doanh số trong phân khúc Sedan hạng B càng trở nên hấp dẫn hơn bao giờ hết với sự góp mặt của Toyota Vios, Hyundai Accent, Mazda 2, Kia Soluto.','40',36),(3,'admin','2022-12-30','admin','2022-12-30','HHRV','HR-V sở hữu thiết kế mới nhưng vẫn giữ yếu tố thể thao, đồng thời bổ sung thêm gói an toàn chủ động Honda Sensing, tính năng quản lý xe qua ứng dụng thông minh.','1.498','1.5L VTEC TURBO, 4 kỳ, 4 xi-lanh thằng hàng, tăng áp','PGM-FI (Phun xăng trực tiếp)','Vô cấp CVT','https://drive.google.com/uc?export=view&id=1aZ8Ca4F6UhUiyTFmmOauY7sOf8YLam8j','250',NULL,'240/1.700-4.500','Honda HR-V',699000000,'HR-V sở hữu thiết kế mới nhưng vẫn giữ yếu tố thể thao, đồng thời bổ sung thêm gói an toàn chủ động Honda Sensing, tính năng quản lý xe qua ứng dụng thông minh.','40',36),(4,'admin','2022-12-30','admin','2022-12-30','HCRV','Đây là mẫu xe gầm cao thành công nhất của Honda tại Việt Nam, CR-V có vị trí và tên tuổi trong phân khúc, tuy nhiên 3 năm trở lại đây sự trỗi dậy của xe Hàn cũng khiến CR-V gặp khó trong cuộc đua doanh số.','1.498','VTEC 1.5 turbo I4','Phun xăng điện tử PGM/FI','CVT','https://drive.google.com/uc?export=view&id=189TQb7F1iAy0-Inab7NeocM3HqDzb52e','260',NULL,'240/2000-5000','Honda CR-V',998000000,'Đây là mẫu xe gầm cao thành công nhất của Honda tại Việt Nam, CR-V có vị trí và tên tuổi trong phân khúc, tuy nhiên 3 năm trở lại đây sự trỗi dậy của xe Hàn cũng khiến CR-V gặp khó trong cuộc đua doanh số.','57',35),(5,'admin','2022-12-30','admin','2022-12-30','HA','Được thử nghiệm bởi Trung tâm Thử nghiệm khí thải phương tiện cơ giới đường bộ (NETC), Cục Đăng kiểm Việt Nam theo tiêu chuẩn Phụ lục QTCVN 6785:2015.\r\nĐược kiểm tra và cấp giấy chứng nhận bởi Phòng Chứng nhận Chất lượng xe cơ giới (VAQ), Cục Đăng kiểm Việt Nam.','1.498','1.5L DOHC VTEC TURBO, 4 xi lanh thẳng hàng, 16 van','Phun xăng trực tiếp/PGM-FI','CVT Ứng dụng EARTH DREAMS TECHNOLOGY','https://drive.google.com/uc?export=view&id=1_2NUHPKm54wIwnqSyBL3w3biMGe3krAZ','225',NULL,'260/1.600-5.000','Honda Accord',1319000000,'Được thử nghiệm bởi Trung tâm Thử nghiệm khí thải phương tiện cơ giới đường bộ (NETC), Cục Đăng kiểm Việt Nam theo tiêu chuẩn Phụ lục QTCVN 6785:2015.\r\nĐược kiểm tra và cấp giấy chứng nhận bởi Phòng Chứng nhận Chất lượng xe cơ giới (VAQ), Cục Đăng kiểm Việt Nam.','56',36),(6,'admin','2022-12-30','admin','2022-12-30','HJ640','Mẫu hatchback của Honda đối thủ của Toyota Yaris, Ford Fiesta, Mazda2 Hatchback đã được ra mắt tại Việt nam vào cuối năm 2017. Xe được trang bị động cơ 1.5L công suất 120Ps, hộp số tự động CVT. Kích thước DxRxC: 3955x 1694x 1524mm, chiều dài cơ sở 2530mm, khoảng sáng gầm xe 137mm, bán kính vòng quay tối thiểu 5.4m.','1.497','SOHC i-VTEC 4 xy-lanh thẳng hàng','Phun xăng trực tiếp/PGM-FI','Vô cấp CVT - Ứng dụng Earth Dreams Technology','https://drive.google.com/uc?export=view&id=1-9lSrQMi_tbqmpDYljPBIShiaj885o_g','230',NULL,'145/6.600','Honda Jazz',594000,'Mẫu hatchback của Honda đối thủ của Toyota Yaris, Ford Fiesta, Mazda2 Hatchback đã được ra mắt tại Việt nam vào cuối năm 2017. Xe được trang bị động cơ 1.5L công suất 120Ps, hộp số tự động CVT. Kích thước DxRxC: 3955x 1694x 1524mm, chiều dài cơ sở 2530mm, khoảng sáng gầm xe 137mm, bán kính vòng quay tối thiểu 5.4m.','40',23),(7,'admin','2022-12-30','admin','2022-12-30','HB','Hòa cùng với xu hướng ngoại thất Honda Brio 2023 được thiết kế theo phong cách thể thao của toàn thế giới, Honda Brio 2023 cũng không phải là mẫu xe ngoại lệ. Từ đó, hãng mẹ Honda đã nhanh chóng áp dụng với mẫu xe đình đám đang bán chạy nhất hiện nay. Đáp ứng cao nhất nhu cầu về hình thức thiết kế của khách hàng. Lựa chọn Honda Brio 2023 bạn luôn nhận được sự hài lòng đến hoàn hảo từ hình thức cho đến phong cách thiết kế. ','1.199','1.2L SOHC i-VTEC, 4 Xi lanh thẳng hàng, 16 van','Phun xăng điện tử','Vô cấp CVT, ứng dụng EARTH DREAMS TECHNOLOGY','https://drive.google.com/uc?export=view&id=13QS1eaE5vSe6odLv3f7UUMIosHTp2jdH','230',NULL,'110/4.800','Honda Brio',418000,'Hòa cùng với xu hướng ngoại thất Honda Brio 2023 được thiết kế theo phong cách thể thao của toàn thế giới, Honda Brio 2023 cũng không phải là mẫu xe ngoại lệ. Từ đó, hãng mẹ Honda đã nhanh chóng áp dụng với mẫu xe đình đám đang bán chạy nhất hiện nay. Đáp ứng cao nhất nhu cầu về hình thức thiết kế của khách hàng. Lựa chọn Honda Brio 2023 bạn luôn nhận được sự hài lòng đến hoàn hảo từ hình thức cho đến phong cách thiết kế. ','35',36);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distribution_agent`
--

DROP TABLE IF EXISTS `distribution_agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `distribution_agent` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `branch_id` bigint DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distribution_agent`
--

LOCK TABLES `distribution_agent` WRITE;
/*!40000 ALTER TABLE `distribution_agent` DISABLE KEYS */;
INSERT INTO `distribution_agent` VALUES (1,'admin','2022-12-29','admin','2022-12-29','Cầu giấy',3,'Đại lý phân phối Cầu Giấy');
/*!40000 ALTER TABLE `distribution_agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distribution_history`
--

DROP TABLE IF EXISTS `distribution_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `distribution_history` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `category_id` bigint DEFAULT NULL,
  `count` bigint DEFAULT NULL,
  `distribution_agent_id` bigint DEFAULT NULL,
  `factory_id` bigint DEFAULT NULL,
  `message` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `product_code_list` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `status` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distribution_history`
--

LOCK TABLES `distribution_history` WRITE;
/*!40000 ALTER TABLE `distribution_history` DISABLE KEYS */;
INSERT INTO `distribution_history` VALUES (1,'factory','2022-12-30','agent','2022-12-30',1,5,1,1,'Export 5 product done','HCTR-1,HCTR-10,HCTR-11,HCTR-12,HCTR-13',1),(2,'factory','2022-12-30','agent','2022-12-30',2,5,1,1,'Export 5 product done','HC-1,HC-10,HC-11,HC-12,HC-13',1),(3,'factory','2022-12-30','agent','2022-12-30',3,5,1,1,'Export 5 product done','HHRV-1,HHRV-10,HHRV-11,HHRV-12,HHRV-13',1),(4,'factory','2022-12-30','factory','2022-12-30',2,5,1,1,'Export 5 product done','HC-14,HC-15,HC-16,HC-17,HC-18',0),(5,'factory','2022-12-30','agent','2022-12-30',6,1,1,1,'Export 1 product done','HJ640-1',1),(6,'factory','2022-12-30','factory','2022-12-30',7,3,1,1,'Export 3 product done','HB-1,HB-10,HB-2',0),(7,'factory','2022-12-30','factory','2022-12-30',6,5,1,1,'Export 5 product done','HJ640-10,HJ640-2,HJ640-3,HJ640-4,HJ640-5',0),(8,'factory','2022-12-30','agent','2022-12-30',7,3,1,1,'Export 3 product done','HB-3,HB-4,HB-5',1),(9,'factory','2022-12-30','agent','2022-12-30',4,10,1,1,'Export 10 product done','HCRV-1,HCRV-10,HCRV-11,HCRV-12,HCRV-13,HCRV-14,HCRV-15,HCRV-2,HCRV-3,HCRV-4',1);
/*!40000 ALTER TABLE `distribution_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factory`
--

DROP TABLE IF EXISTS `factory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `factory` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `branch_id` bigint DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `ware_house_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factory`
--

LOCK TABLES `factory` WRITE;
/*!40000 ALTER TABLE `factory` DISABLE KEYS */;
INSERT INTO `factory` VALUES (1,'admin','2022-12-29','admin','2022-12-29','Cầu Giấy',2,'Cơ sở sản xuất Cầu Giấy',1);
/*!40000 ALTER TABLE `factory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL,
  `distribution_agent_id` bigint DEFAULT NULL,
  `factory_id` bigint DEFAULT NULL,
  `status` int DEFAULT NULL,
  `service_center_id` bigint DEFAULT NULL,
  `warranty_count` bigint DEFAULT '0',
  `warranty_period_date` datetime DEFAULT NULL,
  `category_id` bigint DEFAULT NULL,
  `ware_house_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_h3w5r1mx6d0e5c6um32dgyjej` (`code`),
  KEY `FK1mtsbur82frn64de7balymq9s` (`category_id`),
  KEY `FK10n3sc4rce95qvlwsnfa4kbe6` (`ware_house_id`),
  CONSTRAINT `FK10n3sc4rce95qvlwsnfa4kbe6` FOREIGN KEY (`ware_house_id`) REFERENCES `ware_house` (`id`),
  CONSTRAINT `FK1mtsbur82frn64de7balymq9s` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'factory','2022-12-30','service','2022-12-30','HCTR-1',1,1,6,NULL,1,'2025-09-30 12:55:44',1,1),(2,'factory','2022-12-30','factory','2022-12-30','HCTR-2',NULL,1,1,NULL,0,NULL,1,1),(3,'factory','2022-12-30','factory','2022-12-30','HCTR-3',NULL,1,1,NULL,0,NULL,1,1),(4,'factory','2022-12-30','factory','2022-12-30','HCTR-4',NULL,1,1,NULL,0,NULL,1,1),(5,'factory','2022-12-30','factory','2022-12-30','HCTR-5',NULL,1,1,NULL,0,NULL,1,1),(6,'factory','2022-12-30','factory','2022-12-30','HCTR-6',NULL,1,1,NULL,0,NULL,1,1),(7,'factory','2022-12-30','factory','2022-12-30','HCTR-7',NULL,1,1,NULL,0,NULL,1,1),(8,'factory','2022-12-30','factory','2022-12-30','HCTR-8',NULL,1,1,NULL,0,NULL,1,1),(9,'factory','2022-12-30','factory','2022-12-30','HCTR-9',NULL,1,1,NULL,0,NULL,1,1),(10,'factory','2022-12-30','agent','2022-12-30','HCTR-10',1,1,10,NULL,1,'2025-09-30 12:56:16',1,1),(11,'factory','2022-12-30','service','2022-12-30','HCTR-11',1,1,7,NULL,1,'2025-09-30 12:56:30',1,1),(12,'factory','2022-12-30','service','2022-12-30','HCTR-12',1,1,6,NULL,1,'2025-09-30 15:40:26',1,1),(13,'factory','2022-12-30','agent','2022-12-30','HCTR-13',1,1,3,NULL,0,NULL,1,1),(14,'factory','2022-12-30','factory','2022-12-30','HCTR-14',NULL,1,1,NULL,0,NULL,1,1),(15,'factory','2022-12-30','factory','2022-12-30','HCTR-15',NULL,1,1,NULL,0,NULL,1,1),(16,'factory','2022-12-30','factory','2022-12-30','HCTR-16',NULL,1,1,NULL,0,NULL,1,1),(17,'factory','2022-12-30','factory','2022-12-30','HCTR-17',NULL,1,1,NULL,0,NULL,1,1),(18,'factory','2022-12-30','factory','2022-12-30','HCTR-18',NULL,1,1,NULL,0,NULL,1,1),(19,'factory','2022-12-30','factory','2022-12-30','HCTR-19',NULL,1,1,NULL,0,NULL,1,1),(20,'factory','2022-12-30','factory','2022-12-30','HCTR-20',NULL,1,1,NULL,0,NULL,1,1),(21,'factory','2022-12-30','service','2022-12-30','HC-1',1,1,9,NULL,1,'2025-12-30 15:39:23',2,2),(22,'factory','2022-12-30','factory','2022-12-30','HC-2',NULL,1,1,NULL,0,NULL,2,1),(23,'factory','2022-12-30','factory','2022-12-30','HC-3',NULL,1,1,NULL,0,NULL,2,1),(24,'factory','2022-12-30','factory','2022-12-30','HC-4',NULL,1,1,NULL,0,NULL,2,1),(25,'factory','2022-12-30','factory','2022-12-30','HC-5',NULL,1,1,NULL,0,NULL,2,1),(26,'factory','2022-12-30','factory','2022-12-30','HC-6',NULL,1,1,NULL,0,NULL,2,1),(27,'factory','2022-12-30','factory','2022-12-30','HC-7',NULL,1,1,NULL,0,NULL,2,1),(28,'factory','2022-12-30','factory','2022-12-30','HC-8',NULL,1,1,NULL,0,NULL,2,1),(29,'factory','2022-12-30','factory','2022-12-30','HC-9',NULL,1,1,NULL,0,NULL,2,1),(30,'factory','2022-12-30','agent','2022-12-30','HC-10',1,1,4,NULL,0,'2025-12-30 15:39:46',2,2),(31,'factory','2022-12-30','agent','2022-12-30','HC-11',1,1,4,NULL,0,'2025-12-30 15:56:46',2,2),(32,'factory','2022-12-30','agent','2022-12-30','HC-12',1,1,4,NULL,0,'2025-12-30 16:03:20',2,2),(33,'factory','2022-12-30','agent','2022-12-30','HC-13',1,1,3,NULL,0,NULL,2,2),(34,'factory','2022-12-30','factory','2022-12-30','HC-14',NULL,1,2,NULL,0,NULL,2,1),(35,'factory','2022-12-30','factory','2022-12-30','HC-15',NULL,1,2,NULL,0,NULL,2,1),(36,'factory','2022-12-30','factory','2022-12-30','HC-16',NULL,1,2,NULL,0,NULL,2,1),(37,'factory','2022-12-30','factory','2022-12-30','HC-17',NULL,1,2,NULL,0,NULL,2,1),(38,'factory','2022-12-30','factory','2022-12-30','HC-18',NULL,1,2,NULL,0,NULL,2,1),(39,'factory','2022-12-30','factory','2022-12-30','HC-19',NULL,1,1,NULL,0,NULL,2,1),(40,'factory','2022-12-30','factory','2022-12-30','HC-20',NULL,1,1,NULL,0,NULL,2,1),(41,'factory','2022-12-30','agent','2022-12-30','HHRV-1',1,1,3,NULL,0,NULL,3,2),(42,'factory','2022-12-30','factory','2022-12-30','HHRV-2',NULL,1,1,NULL,0,NULL,3,1),(43,'factory','2022-12-30','factory','2022-12-30','HHRV-3',NULL,1,1,NULL,0,NULL,3,1),(44,'factory','2022-12-30','factory','2022-12-30','HHRV-4',NULL,1,1,NULL,0,NULL,3,1),(45,'factory','2022-12-30','factory','2022-12-30','HHRV-5',NULL,1,1,NULL,0,NULL,3,1),(46,'factory','2022-12-30','factory','2022-12-30','HHRV-6',NULL,1,1,NULL,0,NULL,3,1),(47,'factory','2022-12-30','factory','2022-12-30','HHRV-7',NULL,1,1,NULL,0,NULL,3,1),(48,'factory','2022-12-30','factory','2022-12-30','HHRV-8',NULL,1,1,NULL,0,NULL,3,1),(49,'factory','2022-12-30','factory','2022-12-30','HHRV-9',NULL,1,1,NULL,0,NULL,3,1),(50,'factory','2022-12-30','agent','2022-12-30','HHRV-10',1,1,3,NULL,0,NULL,3,2),(51,'factory','2022-12-30','agent','2022-12-30','HHRV-11',1,1,3,NULL,0,NULL,3,2),(52,'factory','2022-12-30','agent','2022-12-30','HHRV-12',1,1,3,NULL,0,NULL,3,2),(53,'factory','2022-12-30','agent','2022-12-30','HHRV-13',1,1,3,NULL,0,NULL,3,2),(54,'factory','2022-12-30','factory','2022-12-30','HHRV-14',NULL,1,1,NULL,0,NULL,3,1),(55,'factory','2022-12-30','factory','2022-12-30','HHRV-15',NULL,1,1,NULL,0,NULL,3,1),(56,'factory','2022-12-30','agent','2022-12-30','HCRV-1',1,1,10,NULL,1,'2025-11-30 15:43:13',4,2),(57,'factory','2022-12-30','agent','2022-12-30','HCRV-2',1,1,5,NULL,1,'2025-11-30 15:43:16',4,2),(58,'factory','2022-12-30','service','2022-12-30','HCRV-3',1,1,7,NULL,1,'2025-11-30 15:43:18',4,2),(59,'factory','2022-12-30','agent','2022-12-30','HCRV-4',1,1,3,NULL,0,NULL,4,2),(60,'factory','2022-12-30','factory','2022-12-30','HCRV-5',NULL,1,1,NULL,0,NULL,4,1),(61,'factory','2022-12-30','factory','2022-12-30','HCRV-6',NULL,1,1,NULL,0,NULL,4,1),(62,'factory','2022-12-30','factory','2022-12-30','HCRV-7',NULL,1,1,NULL,0,NULL,4,1),(63,'factory','2022-12-30','factory','2022-12-30','HCRV-8',NULL,1,1,NULL,0,NULL,4,1),(64,'factory','2022-12-30','factory','2022-12-30','HCRV-9',NULL,1,1,NULL,0,NULL,4,1),(65,'factory','2022-12-30','agent','2022-12-30','HCRV-10',1,1,3,NULL,0,NULL,4,2),(66,'factory','2022-12-30','agent','2022-12-30','HCRV-11',1,1,3,NULL,0,NULL,4,2),(67,'factory','2022-12-30','agent','2022-12-30','HCRV-12',1,1,3,NULL,0,NULL,4,2),(68,'factory','2022-12-30','agent','2022-12-30','HCRV-13',1,1,3,NULL,0,NULL,4,2),(69,'factory','2022-12-30','agent','2022-12-30','HCRV-14',1,1,3,NULL,0,NULL,4,2),(70,'factory','2022-12-30','agent','2022-12-30','HCRV-15',1,1,3,NULL,0,NULL,4,2),(71,'factory','2022-12-30','factory','2022-12-30','HA-1',NULL,1,1,NULL,0,NULL,5,1),(72,'factory','2022-12-30','factory','2022-12-30','HA-2',NULL,1,1,NULL,0,NULL,5,1),(73,'factory','2022-12-30','factory','2022-12-30','HA-3',NULL,1,1,NULL,0,NULL,5,1),(74,'factory','2022-12-30','factory','2022-12-30','HA-4',NULL,1,1,NULL,0,NULL,5,1),(75,'factory','2022-12-30','factory','2022-12-30','HA-5',NULL,1,1,NULL,0,NULL,5,1),(76,'factory','2022-12-30','factory','2022-12-30','HA-6',NULL,1,1,NULL,0,NULL,5,1),(77,'factory','2022-12-30','factory','2022-12-30','HA-7',NULL,1,1,NULL,0,NULL,5,1),(78,'factory','2022-12-30','factory','2022-12-30','HA-8',NULL,1,1,NULL,0,NULL,5,1),(79,'factory','2022-12-30','factory','2022-12-30','HA-9',NULL,1,1,NULL,0,NULL,5,1),(80,'factory','2022-12-30','factory','2022-12-30','HA-10',NULL,1,1,NULL,0,NULL,5,1),(81,'factory','2022-12-30','factory','2022-12-30','HA-11',NULL,1,1,NULL,0,NULL,5,1),(82,'factory','2022-12-30','factory','2022-12-30','HA-12',NULL,1,1,NULL,0,NULL,5,1),(83,'factory','2022-12-30','factory','2022-12-30','HA-13',NULL,1,1,NULL,0,NULL,5,1),(84,'factory','2022-12-30','factory','2022-12-30','HA-14',NULL,1,1,NULL,0,NULL,5,1),(85,'factory','2022-12-30','factory','2022-12-30','HA-15',NULL,1,1,NULL,0,NULL,5,1),(86,'factory','2022-12-30','factory','2022-12-30','HA-16',NULL,1,1,NULL,0,NULL,5,1),(87,'factory','2022-12-30','factory','2022-12-30','HA-17',NULL,1,1,NULL,0,NULL,5,1),(88,'factory','2022-12-30','factory','2022-12-30','HA-18',NULL,1,1,NULL,0,NULL,5,1),(89,'factory','2022-12-30','factory','2022-12-30','HA-19',NULL,1,1,NULL,0,NULL,5,1),(90,'factory','2022-12-30','factory','2022-12-30','HA-20',NULL,1,1,NULL,0,NULL,5,1),(91,'factory','2022-12-30','factory','2022-12-30','HA-21',NULL,1,1,NULL,0,NULL,5,1),(92,'factory','2022-12-30','factory','2022-12-30','HA-22',NULL,1,1,NULL,0,NULL,5,1),(93,'factory','2022-12-30','factory','2022-12-30','HA-23',NULL,1,1,NULL,0,NULL,5,1),(94,'factory','2022-12-30','factory','2022-12-30','HA-24',NULL,1,1,NULL,0,NULL,5,1),(95,'factory','2022-12-30','factory','2022-12-30','HA-25',NULL,1,1,NULL,0,NULL,5,1),(96,'factory','2022-12-30','factory','2022-12-30','HA-26',NULL,1,1,NULL,0,NULL,5,1),(97,'factory','2022-12-30','factory','2022-12-30','HA-27',NULL,1,1,NULL,0,NULL,5,1),(98,'factory','2022-12-30','factory','2022-12-30','HA-28',NULL,1,1,NULL,0,NULL,5,1),(99,'factory','2022-12-30','factory','2022-12-30','HA-29',NULL,1,1,NULL,0,NULL,5,1),(100,'factory','2022-12-30','factory','2022-12-30','HA-30',NULL,1,1,NULL,0,NULL,5,1),(101,'factory','2022-12-30','factory','2022-12-30','HA-31',NULL,1,1,NULL,0,NULL,5,1),(102,'factory','2022-12-30','factory','2022-12-30','HA-32',NULL,1,1,NULL,0,NULL,5,1),(103,'factory','2022-12-30','factory','2022-12-30','HA-33',NULL,1,1,NULL,0,NULL,5,1),(104,'factory','2022-12-30','factory','2022-12-30','HA-34',NULL,1,1,NULL,0,NULL,5,1),(105,'factory','2022-12-30','factory','2022-12-30','HA-35',NULL,1,1,NULL,0,NULL,5,1),(106,'factory','2022-12-30','agent','2022-12-30','HJ640-1',1,1,3,NULL,0,NULL,6,2),(107,'factory','2022-12-30','factory','2022-12-30','HJ640-2',NULL,1,2,NULL,0,NULL,6,1),(108,'factory','2022-12-30','factory','2022-12-30','HJ640-3',NULL,1,2,NULL,0,NULL,6,1),(109,'factory','2022-12-30','factory','2022-12-30','HJ640-4',NULL,1,2,NULL,0,NULL,6,1),(110,'factory','2022-12-30','factory','2022-12-30','HJ640-5',NULL,1,2,NULL,0,NULL,6,1),(111,'factory','2022-12-30','factory','2022-12-30','HJ640-6',NULL,1,1,NULL,0,NULL,6,1),(112,'factory','2022-12-30','factory','2022-12-30','HJ640-7',NULL,1,1,NULL,0,NULL,6,1),(113,'factory','2022-12-30','factory','2022-12-30','HJ640-8',NULL,1,1,NULL,0,NULL,6,1),(114,'factory','2022-12-30','factory','2022-12-30','HJ640-9',NULL,1,1,NULL,0,NULL,6,1),(115,'factory','2022-12-30','factory','2022-12-30','HJ640-10',NULL,1,2,NULL,0,NULL,6,1),(116,'factory','2022-12-30','factory','2022-12-30','HB-1',NULL,1,2,NULL,0,NULL,7,1),(117,'factory','2022-12-30','factory','2022-12-30','HB-2',NULL,1,2,NULL,0,NULL,7,1),(118,'factory','2022-12-30','agent','2022-12-30','HB-3',1,1,4,NULL,0,'2025-12-30 15:42:56',7,2),(119,'factory','2022-12-30','agent','2022-12-30','HB-4',1,1,4,NULL,0,'2025-12-30 15:43:00',7,2),(120,'factory','2022-12-30','agent','2022-12-30','HB-5',1,1,3,NULL,0,NULL,7,2),(121,'factory','2022-12-30','factory','2022-12-30','HB-6',NULL,1,1,NULL,0,NULL,7,1),(122,'factory','2022-12-30','factory','2022-12-30','HB-7',NULL,1,1,NULL,0,NULL,7,1),(123,'factory','2022-12-30','factory','2022-12-30','HB-8',NULL,1,1,NULL,0,NULL,7,1),(124,'factory','2022-12-30','factory','2022-12-30','HB-9',NULL,1,1,NULL,0,NULL,7,1),(125,'factory','2022-12-30','factory','2022-12-30','HB-10',NULL,1,2,NULL,0,NULL,7,1);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `production_history`
--

DROP TABLE IF EXISTS `production_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `production_history` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `category_id` bigint DEFAULT NULL,
  `count` bigint DEFAULT NULL,
  `factory_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `production_history`
--

LOCK TABLES `production_history` WRITE;
/*!40000 ALTER TABLE `production_history` DISABLE KEYS */;
INSERT INTO `production_history` VALUES (1,'factory','2022-12-30','factory','2022-12-30',1,20,1),(2,'factory','2022-12-30','factory','2022-12-30',2,20,1),(3,'factory','2022-12-30','factory','2022-12-30',3,15,1),(4,'factory','2022-12-30','factory','2022-12-30',4,15,1),(5,'factory','2022-12-30','factory','2022-12-30',5,35,1),(6,'factory','2022-12-30','factory','2022-12-30',6,10,1),(7,'factory','2022-12-30','factory','2022-12-30',7,10,1);
/*!40000 ALTER TABLE `production_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refreshtoken`
--

DROP TABLE IF EXISTS `refreshtoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `refreshtoken` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `modifiedby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `token` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_or156wbneyk8noo4jstv55ii3` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refreshtoken`
--

LOCK TABLES `refreshtoken` WRITE;
/*!40000 ALTER TABLE `refreshtoken` DISABLE KEYS */;
INSERT INTO `refreshtoken` VALUES (1,NULL,NULL,'admin','2022-12-30 00:00:00','2023-01-29 15:01:49','034f967d-b97d-4453-b612-ddc5c2415f28'),(2,NULL,NULL,'factory','2022-12-30 00:00:00','2023-01-29 15:37:19','b1f3a530-5008-4257-b94e-8b9e520e1065'),(3,NULL,NULL,'service','2022-12-30 00:00:00','2023-01-29 15:46:53','651b8237-4173-490f-baae-edb41ab48dba'),(4,NULL,NULL,'agent','2022-12-30 00:00:00','2023-01-29 15:37:00','1fa47e6f-bdd0-4d58-a69a-e9082e01b176');
/*!40000 ALTER TABLE `refreshtoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ADMIN','ADMIN'),(2,'DISTRIBUTION_AGENT','DISTRIBUTION AGENT'),(3,'FACTORY','FACTORY'),(4,'SERVICE_CENTER','SERVICE CENTER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_center`
--

DROP TABLE IF EXISTS `service_center`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service_center` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `branch_id` bigint DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_center`
--

LOCK TABLES `service_center` WRITE;
/*!40000 ALTER TABLE `service_center` DISABLE KEYS */;
INSERT INTO `service_center` VALUES (1,'admin','2022-12-29','admin','2022-12-29','Cầu giấy',1,'Trung tâm bảo hành Cầu Giấy');
/*!40000 ALTER TABLE `service_center` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_history`
--

DROP TABLE IF EXISTS `transaction_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_history` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `customer_age` bigint DEFAULT NULL,
  `customer_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `distribution_agent_id` bigint DEFAULT NULL,
  `product_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_history`
--

LOCK TABLES `transaction_history` WRITE;
/*!40000 ALTER TABLE `transaction_history` DISABLE KEYS */;
INSERT INTO `transaction_history` VALUES (1,'agent','2022-12-30','agent','2022-12-30',NULL,'Nguyễn Văn Linh',1,1),(2,'agent','2022-12-30','agent','2022-12-30',NULL,'Hoàng Minh Dương',1,10),(3,'agent','2022-12-30','agent','2022-12-30',NULL,'Đào Quang Huy',1,11),(4,'agent','2022-12-30','agent','2022-12-30',NULL,'duong',1,21),(5,'agent','2022-12-30','agent','2022-12-30',NULL,'duong2',1,30),(6,'agent','2022-12-30','agent','2022-12-30',NULL,'adas',1,12),(7,'agent','2022-12-30','agent','2022-12-30',NULL,'adfg',1,118),(8,'agent','2022-12-30','agent','2022-12-30',NULL,'dấdasda',1,119),(9,'agent','2022-12-30','agent','2022-12-30',NULL,'dấdasadsf',1,56),(10,'agent','2022-12-30','agent','2022-12-30',NULL,'đâsd',1,57),(11,'agent','2022-12-30','agent','2022-12-30',NULL,'đâsd',1,58),(12,'agent','2022-12-30','agent','2022-12-30',NULL,'',1,31),(13,'agent','2022-12-30','agent','2022-12-30',NULL,'adasd',1,32);
/*!40000 ALTER TABLE `transaction_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `modifiedby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL,
  `address` text CHARACTER SET utf8mb3 COLLATE utf8mb3_bin,
  `fullname` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `image` text CHARACTER SET utf8mb3 COLLATE utf8mb3_bin,
  `password` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `phonenumber` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `refresh_token_id` bigint NOT NULL,
  `role_id` bigint DEFAULT NULL,
  `branch_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKo3wsa4hvodpbgd6upolukg7nj` (`refresh_token_id`),
  KEY `FKn82ha3ccdebhokx3a8fgdqeyy` (`role_id`),
  KEY `FK9yy0ya980j002yvtxi9r7kv6b` (`branch_id`),
  CONSTRAINT `FK9yy0ya980j002yvtxi9r7kv6b` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `FKn82ha3ccdebhokx3a8fgdqeyy` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FKo3wsa4hvodpbgd6upolukg7nj` FOREIGN KEY (`refresh_token_id`) REFERENCES `refreshtoken` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,NULL,NULL,NULL,NULL,NULL,'ADMIN',NULL,'$2a$10$fXPNGnniIlAtoBpGQGZBbuCScdj4Pdnj.BLd2lujHr0cYo2z7epzy',NULL,'admin',1,1,NULL),(2,'admin','2022-12-29 00:00:00','admin','2022-12-29 00:00:00',NULL,'LinhNV',NULL,'$2a$10$iUqVINnu3.QHzHil2P7aVu7y0ut.QY7hdrOot03HwZZrBjS6HP/uO','0963776886','factory',2,3,2),(3,'admin','2022-12-29 00:00:00','admin','2022-12-29 00:00:00',NULL,'LinhNV',NULL,'$2a$10$fjYDewByicX/3i2BY6HPce7IdKInjpzxYbbQV2uk23IV906Rixvs.','0963776886','service',3,4,1),(4,'admin','2022-12-29 00:00:00','admin','2022-12-29 00:00:00',NULL,'LinhNV',NULL,'$2a$10$rIc8WQnb21UXL2gAqG1zS.dvo8TBnRRCAWam7Cc9j4vV0aRFxRjbO','0963776886','agent',4,2,3);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ware_house`
--

DROP TABLE IF EXISTS `ware_house`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ware_house` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ware_house`
--

LOCK TABLES `ware_house` WRITE;
/*!40000 ALTER TABLE `ware_house` DISABLE KEYS */;
INSERT INTO `ware_house` VALUES (1,'admin','2022-12-29','admin','2022-12-29','Cầu Giấy','Nhà kho Cầu Giấy'),(2,'agent','2022-12-30','agent','2022-12-30','66 Hồ Tùng Mậu','Nhà kho Hồ Tùng Mậu');
/*!40000 ALTER TABLE `ware_house` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warranty_history`
--

DROP TABLE IF EXISTS `warranty_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `warranty_history` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createdby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `modifiedby` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `modifieddate` date DEFAULT NULL,
  `distribution_agent_id` bigint DEFAULT NULL,
  `error_message` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin DEFAULT NULL,
  `factory_id` bigint DEFAULT NULL,
  `product_id` bigint DEFAULT NULL,
  `service_center_id` bigint DEFAULT NULL,
  `status` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warranty_history`
--

LOCK TABLES `warranty_history` WRITE;
/*!40000 ALTER TABLE `warranty_history` DISABLE KEYS */;
INSERT INTO `warranty_history` VALUES (1,'agent','2022-12-30','service','2022-12-30',1,'Xe bị hỏng vô lăng điều khiển',NULL,1,1,1),(2,'agent','2022-12-30','agent','2022-12-30',1,'Xe bị hỏng động cơ',1,10,1,4),(3,'agent','2022-12-30','service','2022-12-30',1,'hỏng xe',NULL,11,1,2),(4,'agent','2022-12-30','service','2022-12-30',1,'hỏng ổ khóa',NULL,12,1,1),(5,'agent','2022-12-30','agent','2022-12-30',1,'hỏng óc vít',1,56,1,4),(6,'agent','2022-12-30','agent','2022-12-30',1,'hỏng xích',NULL,57,1,0),(7,'agent','2022-12-30','service','2022-12-30',1,'lỗi phần mềm',NULL,58,1,2),(8,'agent','2022-12-30','service','2022-12-30',1,'ghế rách',NULL,21,1,3);
/*!40000 ALTER TABLE `warranty_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-12-30 16:07:57
