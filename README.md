# Đây là Ứng dụng Production Move

- Ứng đụng được úng dụng các công nghệ:
  + Front end: ReactJs, axios, ...
  + Back end: Spring boot, Spring security, Spring data jpa...
  + Database: MySQL
  + Server: Docker

## Trước khi cài đặt

- Đảm bảo máy bạn đã được cài đặt Docker. Nếu chưa hãy cài đặt nó [Docker Desktop Manual](https://docs.docker.com/desktop/).
- Đảm bảo máy bạn đã được cài đặt NodeJs. Nếu chưa hãy cài đặt nó [Node Js](https://nodejs.org/en/download/).

## Khởi động ứng dụng

- Mở ứng dụng Docker được yêu cầu ở trên.
- Vào folder của app và lần lượt nhấn 2 file `run-client.bat` và `run-server.bat`.(Không được tắt cửa sổ console. Và lần đầu chạy có thể hơi lâu!)
- Giao diện sẽ được khởi chạy ở [localhost:3000](http://localhost:3000/) (Tài khoản mặc định của app là admin/admin).
