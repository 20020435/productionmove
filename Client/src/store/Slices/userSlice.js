import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
  name: "user",
  initialState: {
    username: "",
    fullName: "",
    phoneNumber: null,
    image: "",
    address: "",
    role: "",
  },
  reducers: {
    storeUser: function (state, { payload }) {
      let newUser = { ...state, ...payload };
      state.username = newUser.username;
      state.fullName = newUser.fullName;
      state.phoneNumber = newUser.phoneNumber;
      state.image = newUser.image;
      state.address = newUser.address;
      state.role = newUser.role;
    },
    removeUser: function (state) {
      state.username = "";
      state.fullName = "";
      state.phoneNumber = null;
      state.image = "";
      state.address = "";
      state.role = "";
    },
  },
});

export const { storeUser, removeUser } = userSlice.actions;

export default userSlice.reducer;
