import { createSlice } from "@reduxjs/toolkit";

const toastSlice = createSlice({
  name: "toast",
  initialState: {
    array: [],
  },
  reducers: {
    addToast: function (state, { payload }) {
      const newToast = { ...payload, id: state.array.length - 1 };
      state.array = [...state.array, { ...newToast }];
    },
    removeToast: function (state, { payload }) {
      let arr = [...state.array];
      arr.splice(payload, 1);
      state.array = [...arr];
    },
  },
});

export const { addToast, removeToast } = toastSlice.actions;

export default toastSlice.reducer;
