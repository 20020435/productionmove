import { useDispatch } from "react-redux";
import { addToast, removeToast } from "@/store/Slices/toastSlice";

const useToast = () => {
  const dispatch = useDispatch();
  return {
    show: (toast) => {
      dispatch(addToast(toast));
      setTimeout(() => {
        dispatch(removeToast(0));
      }, 5000);
    },
    hide: (id) => {
      dispatch(removeToast(id));
    },
  };
};

export { useToast };
