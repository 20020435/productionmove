import $axios from "@/service/axios";
import {
  getLocalAccessToken,
  setLocalAccessToken,
  setLocalRefreshToken,
} from "@/service/token";
import {
  setCategories,
  setDistributions,
  setProductStatuses,
  setQuantityCategory,
  setServiceCenters,
  setWarehouses,
} from "@/store/Slices/dataSlice";
import {
  storeUser
} from "@/store/Slices/userSlice";
import {
  useDispatch
} from "react-redux";

const useApi = () => {
  const $dispatch = useDispatch();

  const config = {
    headers: {
      Authorization: `Bearer ${getLocalAccessToken()}`,
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  };

  const formDataConfig = {
    headers: {
      Authorization: `Bearer ${getLocalAccessToken()}`,
      "Content-Type": "multipart/form-data",
    },
  };

  return {
    getAccessToken: async (body, remember = false) => {
      let accessToken = "";
      let url = body.refreshToken ? "/auth/refreshtoken" : "/auth/signin";
      await $axios
        .post(url, body, {
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
          },
        })
        .then((response) => {
          accessToken = JSON.stringify(response.data.accessToken);
          setLocalAccessToken(JSON.stringify(response.data.accessToken));
          setLocalRefreshToken(
            remember && JSON.stringify(response.data.refreshToken)
          );
        })
        .catch((error) => console.log(error));
      if (accessToken)
        await $axios
        .get("user/userinfo", config)
        .then((response) => {
          $dispatch(storeUser(response.data));
        })
        .catch((error) => console.log(error));
      return accessToken;
    },
    getUserInfo: async () => {
      let userInfo = {};
      await $axios
        .get("user/userinfo", config)
        .then((response) => {
          userInfo = {
            ...response.data
          };
          $dispatch(storeUser(response.data));
        })
        .catch((error) => console.log(error));
      return userInfo;
    },
    getAllCategory: async () => {
      let data = [];
      await $axios
        .get("category/all", config)
        .then((response) => {
          data = response.data;
          $dispatch(setCategories(data));
        })
        .catch((error) => console.log(error));
      return data;
    },
    getAllDistribution: async () => {
      let data = [];
      await $axios
        .get("distribution-agent/all", config)
        .then((response) => {
          data = response.data;
          $dispatch(setDistributions(data));
        })
        .catch((error) => console.log(error));
      return data;
    },
    getExportHistory: async () => {
      let data = [];
      await $axios
        .get("factory/history/distribution", config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    getProductsHistory: async () => {
      let data = [];
      await $axios
        .get("factory/history/production", config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    getQuantityByCategory: async () => {
      let data = [];
      await $axios
        .get("factory/product", config)
        .then((response) => {
          data = response.data;
          $dispatch(setQuantityCategory(data));
        })
        .catch((error) => console.log(error));
      return data;
    },
    getProductsExported: async (body) => {
      let data = [];
      if (!body) body = {};
      await $axios
        .post("factory/statistical/sell-product", body, config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    getProductStatuses: async () => {
      let data = [];
      await $axios
        .get("product/status/all", config)
        .then((response) => {
          data = response.data;
          data.shift();
          $dispatch(setProductStatuses(data));
        })
        .catch((error) => console.log(error));
      return data;
    },
    getProductByStatus: async (body) => {
      let data = [];
      let productStatuses = [];
      await $axios
        .get("product/status/all", config)
        .then((response) => {
          productStatuses = response.data;
          productStatuses.shift();
          $dispatch(setProductStatuses(productStatuses));
        })
        .catch((error) => console.log(error));
      const defaultStatuses =
        productStatuses.length === 0 ?
        [1] :
        [...productStatuses.map((item) => item.id)];
      if (!body) body = {
        statuses: defaultStatuses
      };
      if (!body.statuses || body.statuses.length === 0)
        body.statuses = defaultStatuses;
      await $axios
        .post("factory/statistical/status", body, config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    getBugRate: async (body) => {
      let data = [];
      if (!body) body = {};
      await $axios
        .post("factory/statistical/error-product", body, config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    getNumberOfProducts: async (body) => {
      let data = [];
      if (!body) body = {};
      await $axios
        .post("distribution-agent/statistical/sell-product", body, config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    getBugProducts: async () => {
      let data = [];
      await $axios
        .get("factory/error-product", config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    getWarrantyOrders: async () => {
      let data = [];
      await $axios
        .get("service-center/order-warranty", config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    getInsuringProducts: async () => {
      let data = [];
      await $axios
        .get("service-center/warranty/all", config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    createProducts: async (body) => {
      let success = false;
      await $axios
        .post("product", body, config)
        .then(() => {
          success = true;
        })
        .catch((error) => console.log(error));
      return success;
    },
    acceptWarrantyOrder: async (id) => {
      let success = false;
      await $axios
        .put(`service-center/order-warranty/${id}`, {}, config)
        .then((response) => {
          let data = response.data;
          success = data !== "Failed";
        })
        .catch((error) => console.log(error));
      return success;
    },
    resolveFixedProduct: async (id) => {
      let success = false;
      await $axios
        .put(`service-center/warranty/done/${id}`, {}, config)
        .then((response) => {
          let data = response.data;
          success = data !== "Failed";
        })
        .catch((error) => console.log(error));
      return success;
    },

    acceptErrorProduct: async (id) => {
      let success = false;
      await $axios
        .put(`factory/error-product/${id}`, {}, config)
        .then((response) => {
          let data = response.data;
          success = data !== "Failed";
        })
        .catch((error) => console.log(error));
      return success;
    },
    rejectFixedProduct: async (id) => {
      let success = false;
      await $axios
        .put(`service-center/warranty/error/${id}`, {}, config)
        .then((response) => {
          let data = response.data;
          success = data !== "Failed";
        })
        .catch((error) => console.log(error));
      return success;
    },
    exportProducts: async (body) => {
      let success = false;
      await $axios
        .post("factory/export", body, config)
        .then(() => {
          success = true;
        })
        .catch((error) => console.log(error));
      return success;
    },
    getAllFacilities: async () => {
      let data = [];
      await $axios
        .get("branch/all", config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    createNewProduct: async (body) => {
      let success = false;
      await $axios
        .post("category", body, formDataConfig)
        .then(() => {
          success = true;
        })
        .catch((error) => console.log(error));
      return success;
    },
    createNewFacility: async (body) => {
      let success = false;
      await $axios
        .post("branch", body, config)
        .then(() => {
          success = true;
        })
        .catch((error) => console.log(error));
      return success;
    },
    getProductQuantity: async (type, body) => {
      let data = [];
      await $axios
        .post(`admin/statistical/${type}`, body, config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    getAllUsersFacility: async (id) => {
      let data = [];
      await $axios
        .get(`branch/${id}/user/all`, config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    getWarrantyProducts: async (body) => {
      let data = [];
      if (!body) body = {};
      await $axios
        .post("service-center/statistical/product", body, config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    getDetailOfFacility: async (id) => {
      let data = [];
      await $axios
        .get(`branch/${id}`, config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    createNewUserAccount: async (body) => {
      let success = false;
      await $axios
        .post("user", body, config)
        .then(() => {
          success = true;
        })
        .catch((error) => console.log(error));
      return success;
    },
    getAllWarehouses: async () => {
      let data = [];
      await $axios
        .get("distribution-agent/warehouse/all", config)
        .then((response) => {
          data = [...response.data];
          $dispatch(setWarehouses(data));
        })
        .catch((error) => console.log(error));
      return data;
    },
    createNewWarehouse: async (body) => {
      let success = false;
      await $axios
        .post("distribution-agent/warehouse", body, config)
        .then(() => {
          success = true;
        })
        .catch((error) => console.log(error));
      return success;
    },
    getAllOrders: async () => {
      let data = [];
      await $axios
        .get('distribution-agent/order/all', config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    confirmOrder: async (orderId, warehouseId) => {
      let success = false;
      await $axios
        .put(`distribution-agent/order/${orderId}?warehouseId=${warehouseId}`, {}, config)
        .then(() => {
          success = true;
        })
        .catch((error) => console.log(error));
      return success;
    },
    getWarehousesByCategory: async (categoryId) => {
      let data = [];
      await $axios
        .get(`distribution-agent/product/count?categoryId=${categoryId}`, config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    getAllErrorProducts: async () => {
      let data = [];
      await $axios
        .get('distribution-agent/warranty/error/all', config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    confirmErrorProduct: async (orderId) => {
      let success = false;
      await $axios
        .put(`distribution-agent/warranty/error/${orderId}`, {}, config)
        .then(() => {
          success = true;
        })
        .catch((error) => console.log(error));
      return success;
    },
    getAllWarranty: async () => {
      let data = [];
      await $axios
        .get('distribution-agent/warranty/all', config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    createWarrantyForm: async (body) => {
      let success = {};
      await $axios
        .post("distribution-agent/warranty", body, config)
        .then((response) => {
          success = response;
        })
        .catch((error) => console.log(error));
      return success;
    },
    getAllServiceCenters: async () => {
      let data = [];
      await $axios
        .get("service-center/all", config)
        .then((response) => {
          data = [...response.data];
          $dispatch(setServiceCenters(data));
        })
        .catch((error) => console.log(error));
      return data;
    },
    getAllDoneProducts: async () => {
      let data = [];
      await $axios
        .get('distribution-agent/warranty/done/all', config)
        .then((response) => {
          data = response.data;
        })
        .catch((error) => console.log(error));
      return data;
    },
    confirmDoneProduct: async (orderId) => {
      let success = false;
      await $axios
        .put(`distribution-agent/warranty/done/${orderId}`, {}, config)
        .then(() => {
          success = true;
        })
        .catch((error) => console.log(error));
      return success;
    },
    createPurchaseForm: async (body) => {
      let success = false;
      await $axios
        .post("distribution-agent/payment", body, config)
        .then(() => {
          success = true;
        })
        .catch((error) => console.log(error));
      return success;
    },
    deleteCategory: async (categoryId) => {
      let success = false;
      await $axios
        .delete(`category/${categoryId}`, config)
        .then(() => {
          success = true;
        })
        .catch((error) => console.log(error));
      return success;
    },
    deleteFacility: async (facilityId) => {
      let success = false;
      await $axios
        .delete(`branch/${facilityId}`, config)
        .then(() => {
          success = true;
        })
        .catch((error) => console.log(error));
      return success;
    },
    deleteAccount: async (accountId) => {
      let success = false;
      await $axios
        .delete(`user/${accountId}`, config)
        .then(() => {
          success = true;
        })
        .catch((error) => console.log(error));
      return success;
    },
    deleteWarehouse: async (warehouseId) => {
      let success = false;
      await $axios
        .delete(`distribution-agent/warehouse/${warehouseId}`, config)
        .then(() => {
          success = true;
        })
        .catch((error) => console.log(error));
      return success;
    },
  };
};

export {
  useApi
};