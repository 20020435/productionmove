import { useDispatch } from "react-redux";
import { setLoading } from "@/store/Slices/loadingSlice";

const useLoading = () => {
  const $dispatch = useDispatch();
  return {
    start: () => $dispatch(setLoading(true)),
    stop: () => $dispatch(setLoading(false)),
  };
};

export { useLoading };
