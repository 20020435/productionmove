import classNames from "classnames/bind";
import style from "./PageTitle.module.scss";

const cx = classNames.bind(style);

export default function PageTitle({ text }) {
  return (
    <div
      className={cx("pageTitle")}
    >
      <h1>{text}</h1>
    </div>
  );
}
