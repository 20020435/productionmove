export const phoneNumberRegex = /^\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4,5})$/;
export const strongPasswordRegex =/^[a-zA-Z0-9!"#$%&'()*+,-.\/:;<=>?@[\]^_`{|}~]{8,}$/;