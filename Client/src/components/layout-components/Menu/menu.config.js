import { v4 as uuid4 } from "uuid";
import {
  MdHomeFilled,
  MdDashboard,
  MdSell,
  MdOutlineError,
  MdMedicalServices,
} from "react-icons/md";
import { GrTest } from "react-icons/gr";
import { FiBox } from "react-icons/fi";
import { TbPackgeExport, TbTool } from "react-icons/tb";

import { IoStorefrontSharp, IoBarChartSharp } from "react-icons/io5";
import { HiBuildingOffice2 } from "react-icons/hi2";
import { FaWarehouse } from "react-icons/fa";
import { RiExchangeDollarFill } from "react-icons/ri";
import { TfiViewList } from "react-icons/tfi";
import { RxGear } from "react-icons/rx";
import { AiOutlineFileDone } from "react-icons/ai";

const cases = {
  ADMIN: [
    {
      id: uuid4(),
      title: "Thống kê",
      icon: MdHomeFilled,
      path: "/",
    },
    {
      id: uuid4(),
      title: "Dòng sản phẩm",
      icon: IoStorefrontSharp,
      path: "/admin/products",
    },
    {
      id: uuid4(),
      title: "Cơ sở",
      icon: HiBuildingOffice2,
      path: "/admin/facilities-management",
    },
  ],
  FACTORY: [
    {
      id: uuid4(),
      title: "DashBoard",
      icon: MdDashboard,
      path: "/",
    },
    {
      id: uuid4(),
      title: "Nhập sản phẩm",
      icon: FiBox,
      path: "/products",
    },
    {
      id: uuid4(),
      title: "Xuất sản phẩm",
      icon: TbPackgeExport,
      path: "/export",
    },
    {
      id: uuid4(),
      title: "Sản phẩm lỗi",
      icon: TbTool,
      path: "/insurance",
    },
  ],
  SERVICE_CENTER: [
    {
      id: uuid4(),
      title: "DashBoard",
      icon: MdDashboard,
      path: "/",
    },
    {
      id: uuid4(),
      title: "Đơn yêu cầu",
      icon: TfiViewList,
      path: "/import",
    },
    {
      id: uuid4(),
      title: "Tiến trình",
      icon: RxGear,
      path: "/progressing",
    },
  ],
  DISTRIBUTION_AGENT: [
    {
      id: uuid4(),
      title: "Trang chủ",
      icon: MdHomeFilled,
      path: "/",
    },
    {
      id: uuid4(),
      title: "Nhà kho",
      icon: FaWarehouse,
      path: "/distribution-agent/warehouse",
    },
    {
      id: uuid4(),
      title: "Đơn hàng",
      icon: MdSell,
      path: "/distribution-agent/orders",
    },
    {
      id: uuid4(),
      title: "Bán sản phẩm",
      icon: RiExchangeDollarFill,
      path: "/distribution-agent/purchase",
    },
    {
      id: uuid4(),
      title: "Tạo bảo hành",
      icon: MdMedicalServices,
      path: "/distribution-agent/warranty",
    },
    {
      id: uuid4(),
      title: "Bảo hành thành công",
      icon: AiOutlineFileDone,
      path: "/distribution-agent/done-products",
    },
    {
      id: uuid4(),
      title: "Bảo hành thất bại",
      icon: MdOutlineError,
      path: "/distribution-agent/error-products",
    },
  ],
  default: [],
};

export default function getMenu(role) {
  return cases[role];
}
