import {v4 as idv4} from 'uuid';
import {BiLogOut} from 'react-icons/bi';

export const toolsItem = [
    {
        id: idv4(),
        icon: BiLogOut
    }
]