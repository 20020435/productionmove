import React, { useState } from "react";
import classNames from "classnames/bind";
import style from "./MenuStyle.module.scss";
import { Link } from "react-router-dom";
import AppLogo from "@/assets/images/logo.png";
import { v4 as uuid4 } from "uuid";
import { useLogin } from "@/hooks/useLogin";
import getMenu from "./menu.config";
import { useSelector } from "react-redux";
import { toolsItem } from "./tools.config";
import { Modal } from "antd";

const cx = classNames.bind(style);

const Menu = () => {
  const [page, setPage] = useState(window.location.pathname);
  const user = useSelector((state) => state.user);
  const menu = getMenu(user.role);
  const $login = useLogin();

  const [isOpenConfirmModal, setIsOpenConfirmModal] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState(
    "Bạn chắc chắn muốn xóa đăng xuất?"
  );

  const handleOnClickTool = () => {
    showModal();
  };

  const showModal = () => {
    setIsOpenConfirmModal(true);
  };

  const handleOK = async () => {
    $login.logOut();
  };

  const handleCancel = () => {
    setIsOpenConfirmModal(false);
  };

  return (
    <div className={cx("sidebar__wrapper")}>
      <div className={cx("logo-brand__wrapper")}>
        <Link
          onClick={() => setPage("/")}
          to={"/"}
          className={cx("logo-brand")}
        >
          <img className={cx("logo")} src={AppLogo} alt="app-logo" />
        </Link>
      </div>

      <div className={cx("divider")}></div>
      <div className={cx("list-links")}>
        {menu.map((item) => {
          const Icon = item.icon;

          return (
            <div key={uuid4()} className={cx("icon__wrapper")}>
              <Link
                onClick={() => setPage(item.path)}
                className={
                  cx("item-icon") +
                  (page === item.path ? " " + cx("current") : "")
                }
                to={item.path}
              >
                <Icon />
              </Link>
              <span className={cx("tooltip")}>{item.title}</span>
            </div>
          );
        })}
      </div>
      <div className={cx("list-tools")}>
        {toolsItem.map((tool) => {
          const Icon = tool.icon;
          return (
            <div
              key={uuid4()}
              className={cx("icon__wrapper")}
              onClick={() => {
                handleOnClickTool();
              }}
            >
              <div className={cx("item-icon")}>
                <Icon className={cx("logout-icon")} />
              </div>
            </div>
          );
        })}

        <Modal
          title="Đăng xuất"
          open={isOpenConfirmModal}
          onOk={handleOK}
          cancelText={"Hủy"}
          okText={"Xác nhận"}
          confirmLoading={confirmLoading}
          onCancel={handleCancel}
          centered={true}
        >
          <p className={cx("modal-text")}>{modalText}</p>
        </Modal>
      </div>
    </div>
  );
};

export default Menu;
