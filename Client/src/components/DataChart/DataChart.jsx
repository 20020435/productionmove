import classNames from "classnames/bind";
import style from "./DataChart.module.scss";
import DataCol from "./DataCol";

const cx = classNames.bind(style);

export default function DataChart({
  width,
  height,
  data,
  maxValue,
  nameField,
  valueField,
  unit,
}) {
  return (
    <ul
      style={{ width: width || "100%", height: height || "100%" }}
      className={cx("data-chart")}
    >
      {data.map((element, idx) => (
        <li key={idx} className={cx("data-col")}>
          <DataCol value={element.value} maxValue={maxValue} unit={unit} />
          <div className={cx("data-name")}>{element.name}</div>
        </li>
      ))}
      <div className={cx("name-field")}>
        <h4>{nameField}</h4>
      </div>
      <div className={cx("value-field")}>
        <h4>{valueField}</h4>
      </div>
    </ul>
  );
}
