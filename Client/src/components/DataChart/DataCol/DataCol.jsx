import classNames from "classnames/bind";
import style from "./DataCol.module.scss";

const cx = classNames.bind(style);

export default function DataCol({ value, maxValue, unit }) {
  return (
    <div className={cx("data-col")}>
      <h4>
        {value}
        {unit}
      </h4>
      <div
        style={{ height: (value * 100) / maxValue + "%" }}
        className={cx("value")}
      ></div>
    </div>
  );
}
