import classNames from "classnames/bind";
import style from "./Card.module.scss";

const cx = classNames.bind(style);

export default function Card({ children, width, justify}) {
  return (
    <div
      className={cx("card")}
      style={{
        width: `calc(${width} - 40px)`,
        alignItems: justify || "flex-start",
      }}
    >
      {children}
    </div>
  );
}
