import classNames from "classnames/bind";
import { forwardRef, useImperativeHandle, useState } from "react";
import { useSelector } from "react-redux";
import style from "./FormItem.module.scss";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleDown, faAngleUp } from "@fortawesome/free-solid-svg-icons";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { TextField } from "@mui/material";

library.add(faAngleDown, faAngleUp);
const cx = classNames.bind(style);

const FormItem = forwardRef(({ item, onchange, onblur }, ref) => {
  const cases = {
    checkbox: false,
    textField: "",
    numberField: 0,
    password: "",
    dropList: "",
  };

  var mouseOn = false;
  useImperativeHandle(ref, () => ({
    clear() {
      setInputValue(cases[item.type] || "");
    },
  }));

  const [hide, setHide] = useState(true);
  const [inputValue, setInputValue] = useState(cases[item.type]);
  const data = useSelector((state) => state.data[item.options] || null);
  const $isLoading = useSelector((state) => state.loading.value);

  const choose = (option) => {
    setHide(true);
    setInputValue(option[item.textOptions]);
    onchange(option[item.valueOptions]);
    onblur();
  };

  const filterData = () => {
    return [...data.filter((el) => el[item.textOptions].includes(inputValue))];
  };

  const handleOptionChange = (value) => {
    if (hide) setHide(false);
    setInputValue(value);
    let textValue = "";
    if (value) {
      data.forEach((el) => {
        if (el[item.textOptions] === value) textValue = el[item.valueOptions];
      });
      if (!textValue) textValue = "error";
    }
    onchange(textValue);
  };

  const handleChange = (value) => {
    setInputValue(value);
    onchange(value);
  };

  const cookedDate = (value) => {
    var datearray = String(value).split("/");
    return datearray[1] + "/" + datearray[0] + "/" + datearray[2];
  };

  const dateChange = (value) => {
    if (value) value = value.$d.toLocaleDateString();
    else value = "";
    setInputValue(value);
    onchange(value);
  };

  const handleBlur = () => {
    if (!mouseOn) {
      setHide(true);
      onblur();
    }
  };

  const handleFocus = () => {
    if (hide) setHide(false);
  };

  const hover = () => {
    mouseOn = true;
  };

  const leave = () => {
    mouseOn = false;
  };

  const components = {
    textField: (
      <div className={cx("text-field")}>
        <label>{item.title}</label>
        <div className={cx("input")}>
          {item.icon}
          <input
            className={item.icon ? cx("iconInput") : cx("nonIconIput")}
            type="text"
            disabled={$isLoading}
            onChange={(e) => handleChange(e.target.value)}
            onBlur={onblur}
            value={inputValue}
          ></input>
        </div>
      </div>
    ),
    numberField: (
      <div className={cx("number-field")}>
        <label>{item.title}</label>
        <div className={cx("input")}>
          {item.icon}
          <input
            className={item.icon ? cx("iconInput") : cx("nonIconIput")}
            type="number"
            disabled={$isLoading}
            onChange={(e) => handleChange(e.target.value)}
            onBlur={onblur}
            value={inputValue}
          ></input>
        </div>
      </div>
    ),
    password: (
      <div className={cx("password")}>
        <label>{item.title}</label>
        <div className={cx("input")}>
          {item.icon}
          <input
            className={item.icon ? cx("iconInput") : cx("nonIconIput")}
            type="password"
            disabled={$isLoading}
            onChange={(e) => handleChange(e.target.value)}
            onBlur={onblur}
            value={inputValue}
          ></input>
        </div>
      </div>
    ),
    checkbox: (
      <div className={cx("checkbox")}>
        {!item.rlt && (
          <label style={{ marginRight: "5px" }}>{item.title}</label>
        )}
        <input
          type="checkbox"
          disabled={$isLoading}
          checked={inputValue}
          onChange={(e) => handleChange(e.target.checked)}
        />
        {item.rlt && <label style={{ marginLeft: "5px" }}>{item.title}</label>}
      </div>
    ),
    dropList: (
      <div className={cx("drop-list")}>
        <label>{item.title}</label>
        <div className={cx("input")}>
          {item.icon}
          <input
            className={item.icon ? cx("iconInput") : cx("nonIconIput")}
            type="text"
            disabled={$isLoading}
            value={inputValue}
            onBlur={handleBlur}
            onFocus={handleFocus}
            onChange={(e) => handleOptionChange(e.target.value)}
          ></input>
          <div onClick={() => setHide(!hide)} className={cx("dropdown")}>
            {hide ? (
              <FontAwesomeIcon icon="fa-solid fa-angle-up" />
            ) : (
              <FontAwesomeIcon icon="fa-solid fa-angle-down" />
            )}
          </div>
        </div>
        <ul className={cx("list-choice") + (hide ? " " + cx("hide") : "")}>
          {data ? (
            filterData().map((el, idx) =>
              el ? (
                <li
                  className={cx("choice")}
                  onClick={() => choose(el)}
                  onMouseOver={hover}
                  onMouseLeave={leave}
                  key={idx}
                >
                  {el[item.textOptions]}
                </li>
              ) : (
                <></>
              )
            )
          ) : (
            <li key={"no-key"}>no data</li>
          )}
        </ul>
      </div>
    ),
    pickDate: (
      <div className={cx("pick-date")}>
        <label>{item.title}</label>
        <div className={cx("input")}>
          {item.icon}
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            {
              <DesktopDatePicker
                inputFormat="DD/MM/YYYY"
                onChange={dateChange}
                value={cookedDate(inputValue)}
                renderInput={(params) => {
                  params.inputProps.value = inputValue || "";
                  return <TextField onBlur={onblur} {...params} />;
                }}
              />
            }
          </LocalizationProvider>
        </div>
      </div>
    ),
    spacer: <div></div>,
  };

  return components[item.type] || <div>wrong type!</div>;
});

export default FormItem;
