import { useSelector } from "react-redux";
import { useToast } from "@/hooks/useToast";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faXmark,
  faCircleExclamation,
  faCircleCheck,
} from "@fortawesome/free-solid-svg-icons";
import classNames from "classnames/bind";
import style from "./Toast.module.scss";

const cx = classNames.bind(style);
library.add(faXmark, faCircleExclamation, faCircleCheck);

export default function Toast() {
  const $toast = useToast();
  const toasts = useSelector((state) => state.toast.array);

  return (
    toasts.length > 0 && (
      <ul className={cx("toast")}>
        {toasts.map((toast, index) => (
          <li className={cx("toast-wrapper")} key={index}>
            <div className={cx("toast-item")}>
              <div className={cx("heading") + " " + cx(toast.type)}></div>
              <div className={cx("toast-message")}>
                {toast.type === "error" && (
                  <FontAwesomeIcon
                    size="2x"
                    className={cx(`${toast.type}-c`)}
                    icon="fa-solid fa-circle-exclamation"
                  />
                )}
                {toast.type === "success" && (
                  <FontAwesomeIcon
                    size="2x"
                    className={cx(`${toast.type}-c`)}
                    icon="fa-solid fa-circle-check"
                  />
                )}
                <p>{toast.message}</p>
              </div>
              <FontAwesomeIcon
                size="2x"
                className={cx("toast-close")}
                onClick={() => $toast.hide(index)}
                icon="fa-solid fa-xmark"
              />
            </div>
          </li>
        ))}
      </ul>
    )
  );
}
