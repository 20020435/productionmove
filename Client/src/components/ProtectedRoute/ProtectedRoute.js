import { Navigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { getLocalAccessToken, getLocalRefreshToken } from "@/service/token";
import { useUser } from "@/hooks/useUser";
import { useLoading } from "@/hooks/useLoading";
import { useApi } from "@/hooks/useApi";

export default function ProtectedRoute({ children, page }) {
  const accessToken = useSelector((state) => state.user.accessToken);
  const localAccessToken = getLocalAccessToken();
  const refreshToken = getLocalRefreshToken();

  const $user = useUser();
  const $loading = useLoading();
  const $api = useApi();

  if (accessToken || localAccessToken) return children;
  else {
    if (refreshToken) {
      $loading.start();
      $api
        .getToken({ refreshToken: refreshToken })
        .then(function (response) {
          $user.login(JSON.stringify(response.data.accessToken));
          return children;
        })
        .catch(function (error) {
          console.log(error);
          return <Navigate to="/login" state={page} replace />;
        });
      $loading.stop();
    } else return <Navigate to="/login" state={page} replace />;
  }
}
