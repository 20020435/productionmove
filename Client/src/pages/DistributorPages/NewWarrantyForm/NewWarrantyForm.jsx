import React from "react";
import classNames from "classnames/bind";
import style from "./NewWarrantyForm.module.scss";
import { Form, Input, message, Select, Cascader } from "antd";
import { Link } from "react-router-dom";
import { useApi } from "@/hooks/useApi";
import { useToast } from "@/hooks/useToast";
import { useLoading } from "@/hooks/useLoading";
import Form2 from "@/components/Form";
import { useEffect, useState, useRef } from "react";

const cx = classNames.bind(style);

const NewWarrantyForm = () => {
  const $api = useApi();
  const $loading = useLoading();
  const $toast = useToast();
  const { TextArea } = Input;
  const formElement = useRef(null);

  const getAllServiceCenters = async () => {
    await $api.getAllServiceCenters();
  };

  useEffect(() => {
    getAllServiceCenters();
  }, []);

  const [form, setForm] = useState({
    serviceCenterId: "",
  });

  const formItems = [
    {
      type: "dropList",
      name: "serviceCenterId",
      options: "serviceCenters",
      title: 'Trung tâm bảo hành',
      textOptions: "name",
      valueOptions: "id",
      rules: [
        (v) => (v ? "" : "Vui lòng chọn trung tâm bảo hành"),
        (v) => (v !== "error" ? "" : "Trung tâm bảo hành không hợp lệ"),
      ],
      col: 12,
    },
  ];

  const handleCreateWarrantyForm = async () => {
    const productCode = document.querySelector('input[name = "warrantyProductCode"]').value;
    const errorMessage = document.querySelector('textarea[name = "warrantyErrorMessage"]').value;
    
    const formCreate = document.querySelector('form[id = "newWarranty-form"]');

    if(formCreate) {
        const body = {
            productCode,
            serviceCenterId: form.serviceCenterId,
            errorMessage,
        }
        console.log(form.serviceCenterId);

        let success = await $api.createWarrantyForm(body);
        if (success.status == 200 && success.data === 'Success!!!') {
            console.log(success);
            $toast.show({
              type: "success",
              message: `Tạo thành công đơn bảo hành cho mã sản phẩm ${productCode}!`,
          });
          formCreate.reset();
          } else if(success.status == 400) {
            console.log('400');
            $toast.show({
                type: 'error',
                message: `Mã sản phẩm ${productCode} đã hết hạn bảo hành`
            });
          }
          
          else {
              $toast.show({
                  type: 'error',
                  message: 'Có lỗi xảy ra khi tạo đơn bảo hành!'
              })
          }
    }
  }

  return (
    <div className={cx("distributor-wrap")}>
      <h1 className={cx("distributor__heading")}>Tạo đơn bảo hành mới</h1>
      <Link to={"/distribution-agent/warranty"}>
        <button className={cx("distributor__btn", "distributor__btn--left")}>
          Quay lại
        </button>
      </Link>

      <div className={cx("distributor-form")}>
        <Form
          id="newWarranty-form"
          name="basic"
          layout="vertical"
          initialValues={{ remember: true }}
          autoComplete="off"
        >
          <Form.Item
          className={cx('form-item-custom')}
            label={<p className={cx("distributor-form-label")}>Mã sản phẩm</p>}
            name="warrantyProductCode"
            rules={[{ required: true, message: "Vui lòng nhập mã sản phẩm!" }]}
          >
            <Input
              className={cx("distributor-form-input")}
              placeholder="Nhập mã sản phẩm..."
              name="warrantyProductCode"
            />
          </Form.Item>

          <Form.Item
            name="warrantyServiceCenter"
          >
            <Form2 className = {cx('form2')} ref = {formElement} form = {form} items = {formItems} />
          </Form.Item>

          <Form.Item
            className={cx('form-item-custom')}
            label={<p className={cx("distributor-form-label")}>Thông tin lỗi</p>}
            name="warrantyErrorMessage"
            rules={[{ required: true, message: "Vui lòngthông tin lỗi!" }]}
          >
            <TextArea
              className={cx("distributor-form-input")}
              placeholder="Nhập thông tin lỗi..."
              name="warrantyErrorMessage"
            />
          </Form.Item>

          <Form.Item>
            <button
              className={cx("distributor-submit-btn")}
              type="submit"
              onClick={handleCreateWarrantyForm}
            >
                Tạo đơn bảo hành
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default NewWarrantyForm;
