import { useApi } from "@/hooks/useApi";
import {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useLayoutEffect,
  useRef,
  useState,
} from "react";
import classNames from "classnames/bind";
import style from "./Warranty.module.scss";
import { SearchOutlined } from "@ant-design/icons";
import { useToast } from "@/hooks/useToast";
import { useLoading } from "@/hooks/useLoading";
import { Button, Input, Space, Table } from "antd";
import { Link } from 'react-router-dom';
import Highlighter from "react-highlight-words";

const cx = classNames.bind(style);

const Warranty = () => {
    const $api = useApi();
    const [warranty, setWarranty] = useState([]);

    const getAllWarranty = async () => {
        const data = await $api.getAllWarranty();
        setWarranty(data);
    }

    useEffect(() => {
        getAllWarranty();
    }, []);

    const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef(null);
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
      close,
    }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{
              width: 100,
            }}
          >
            Tìm kiếm
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
            }}
          >
            Làm mới
          </Button>

          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
            Đóng
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const columns = [
    {
        title: "Id",
        dataIndex: "id",
        key: "id",
    },
    {
        title: 'Mã sản phẩm',
        dataIndex: 'code',
        key: 'code',
    },
    {
        title: 'Tên sản phẩm',
        dataIndex: '',
        key: '',
        render: (item) => <div>{item.category.name}</div>
    },
    {
        title: 'Thông tin lỗi',
        dataIndex: 'errorMessage',
        key: 'errorMessage',
        ...getColumnSearchProps('errorMessage'),
    },
    {
        title: 'Trạng thái',
        dataIndex: 'status',
        key: 'status',
        ...getColumnSearchProps('status'),
    },

  ]




    return (
        <div className= {cx("distributor-wrap")}>
            <h1 className={cx('distributor__heading')}>Lịch sử các đơn yêu cầu bảo hành</h1>
            <div>
                    <Link to = {'/distribution-agent/warranty/new'}>
                        <button
                            className={cx('distributor__btn', 'distributor__btn--right')}
                        >
                            Thêm đơn bảo hành
                        </button>
                    </Link>

                    <Table
                        className={cx('distributor__table')}
                        columns = {columns}
                        dataSource = {warranty}
                        pagination = {{pageSize: 5}}
                        rowKey = 'id'
                    >

                        
                    </Table>
                </div>
        </div>
    )
}


export default Warranty;