import { useState, useRef, useLayoutEffect, useEffect } from "react";
import { SearchOutlined } from "@ant-design/icons";
import { Button, Input, Space, Table, Modal, Cascader } from "antd";
import Highlighter from "react-highlight-words";
import { useApi } from "@/hooks/useApi";
import classNames from "classnames/bind";
import style from "./DoneProducts.module.scss";
import { Link } from "react-router-dom";
import Utils from "@/utils";
import Form from "@/components/Form";
import { useToast } from "@/hooks/useToast";

const cx = classNames.bind(style);

const DoneProducts = () => {
  const $api = useApi();
  const $toast = useToast();
  const [doneProducts, setDoneProducts] = useState([]);
  const [isOpenConfirmModal, setIsOpenConfirmModal] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState("Bạn chắc chắn muốn xác nhận?");
  const [orderId, setOrderId] = useState("");

  const getAllDoneProducts = async () => {
    const data = await $api.getAllDoneProducts();
    setDoneProducts(data);
  };

  useEffect(() => {
    getAllDoneProducts();
  }, []);

  const handleConfirm = (orderId) => {
    console.log(orderId);
    showModal();
    setOrderId(orderId);
  };

  const showModal = () => {
    setIsOpenConfirmModal(true);
  };

  const handleOK = async () => {
    setModalText("Đang xác nhận...");
    let success = await $api.confirmDoneProduct(orderId);
    if (success) {
      getAllDoneProducts();
      setIsOpenConfirmModal(false);
      setConfirmLoading(false);
      setModalText("Bạn chắc chắn muốn xác nhận?");
      $toast.show({
        type: "success",
        message: `Trả thành công về cơ sở sản xuất!`,
      });
    } else {
      $toast.show({
        type: "error",
        message: "Có lỗi xảy ra!",
      });
      setModalText("Xác nhận thất bại!");
    }
  };

  const handleCancel = () => {
    setIsOpenConfirmModal(false);
  };

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef(null);
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
      close,
    }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{
              width: 100,
            }}
          >
            Tìm kiếm
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
            }}
          >
            Làm mới
          </Button>

          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
            Đóng
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const columns = [
    {
      title: "Id",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Mã sản phẩm",
      dataIndex: "code",
      key: "code",
      ...getColumnSearchProps("code"),
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "",
      key: "",
      render: (_, item) => <div>{item.category.name}</div>,
    },
    {
      title: "Thông tin lỗi",
      dataIndex: "errorMessage",
      key: "errorMessage",
      ...getColumnSearchProps("errorMessage"),
    },
    {
      title: "Hành động",
      dataIndex: "",
      key: "",
      render: (item) => (
        <div
          className={cx("table__confirm-btn")}
          onClick={() => handleConfirm(item.id)}
        >
          Xác nhận
        </div>
      ),
    },
  ];

  return (
    <div className={cx("distributor-wrap")}>
      <h1 className={cx("distributor__heading")}>
        Danh sách các đơn sản phẩm đã sửa xong
      </h1>
      <Table
        className={cx("distributor__table", "done-products__table")}
        columns={columns}
        dataSource={doneProducts}
        pagination={{ pageSize: 5 }}
      />
      <Modal
        title="Xác nhận trả hàng lỗi về cơ sở sản xuất"
        open={isOpenConfirmModal}
        onOk={handleOK}
        cancelText={"Hủy"}
        okText={"Xác nhận"}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        centered={true}
      >
        <p className={cx("modal-text")}>{modalText}</p>
      </Modal>
    </div>
  );
};

export default DoneProducts;
