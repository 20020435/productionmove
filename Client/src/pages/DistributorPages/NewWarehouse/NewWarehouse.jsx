import React from "react";
import classNames from "classnames/bind";
import style from "./NewWarehouse.module.scss";
import { Form, Input, message, Select, Cascader } from "antd";
import { Link } from "react-router-dom";
import { useApi } from "@/hooks/useApi";
import { useToast } from "@/hooks/useToast";
import { useLoading } from "@/hooks/useLoading";

const cx = classNames.bind(style);

const newWarehouse = () => {
  const $api = useApi();
  const $toast = useToast();
  const $loading = useLoading();

  const handleCreateWarehouse = async () => {
    $loading.start();
    const name = document.querySelector('input[name = "warehouseName"]').value;
    const address = document.querySelector(
      'input[name = "warehouseAddress"]'
    ).value;

    const form = document.querySelector('form[id = "distributor-form"]');
    if(form) {
        console.log('form');
        const body = {
            name,
            address,
        }

        let success = await $api.createNewWarehouse(body);
        if (success) {
            $toast.show({
              type: "success",
              message: `Thêm thành công nhà kho ${name}!`,
          });
          form.reset();
          } else {
              $toast.show({
                  type: 'error',
                  message: 'Có lỗi xảy ra khi thêm nhà kho!'
              })
          }
    }
    $loading.stop();
  };

  return (
    <div className={cx("distributor-wrap")}>
      <h1 className={cx("distributor__heading")}>Thêm nhà kho mới</h1>

      <Link to={"/distribution-agent/warehouse"}>
        <button className={cx("distributor__btn", "distributor__btn--left")}>
          Quay lại
        </button>
      </Link>

      <div className={cx("distributor-form")}>
        <Form
          id="distributor-form"
          name="basic"
          layout="horizontal"
          initialValues={{ remember: true }}
          autoComplete="off"
        >
          <Form.Item
            label={<p className={cx("distributor-form-label")}>Tên nhà kho</p>}
            name="warehouseName"
            rules={[{ required: true, message: "Vui lòng nhập tên nhà kho!" }]}
          >
            <Input
              className={cx("distributor-form-input")}
              placeholder="Nhập tên nhà kho..."
              name="warehouseName"
            />
          </Form.Item>
          <Form.Item
            label={<p className={cx("distributor-form-label")}>Địa chỉ</p>}
            name="warehouseAddress"
            rules={[
              { required: true, message: "Vui lòng nhập địa chỉ nhà kho!" },
            ]}
          >
            <Input
              className={cx("distributor-form-input")}
              type="text"
              placeholder="Nhập địa chỉ..."
              name="warehouseAddress"
            />
          </Form.Item>

          <Form.Item>
            <button
              className={cx("distributor-submit-btn")}
              type="submit"
              onClick={handleCreateWarehouse}
            >
              Thêm nhà kho
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default newWarehouse;
