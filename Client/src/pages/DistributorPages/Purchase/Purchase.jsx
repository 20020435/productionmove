import React from "react";
import classNames from "classnames/bind";
import style from "./Purchase.module.scss";
import { Input, Table, Space, Button, Form } from "antd";
import Highlighter from "react-highlight-words";
import { useApi } from "@/hooks/useApi";
import { useToast } from "@/hooks/useToast";
import { SearchOutlined } from "@ant-design/icons";
import Form2 from "@/components/Form";
import { useEffect, useState, useRef } from "react";

const cx = classNames.bind(style);

const Purchase = () => {
  const $api = useApi();
  const $toast = useToast();
  const selectionType = "radio";
  const [warehouses, setWarehouses] = useState([]);
  const [warehouseId, setWarehouseId] = useState("");

  const [form, setForm] = useState({
    categoryId: "",
  });

  const getWarehouses = async () => {
    const data = await $api.getWarehousesByCategory(form.categoryId);
    setWarehouses(data);
  };

  useEffect(() => {
    $api.getAllCategory();
  }, []);

  const formItems = [
    {
      type: "dropList",
      name: "categoryId",
      title: "Thể loại",
      options: "categories",
      textOptions: "name",
      valueOptions: "id",
      rules: [
        (v) => (v ? "" : "Vui lòng nhập Thể loại"),
        (v) => (v !== "error" ? "" : "Thể loại không hợp lệ"),
      ],
      col: 6,
      onchange: () => {
        getWarehouses();
      },
    },
  ];
  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      setWarehouseId(selectedRows[0].warehouseId);
    },
  };

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");
  const searchInput = useRef(null);
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
      close,
    }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{
              width: 100,
            }}
          >
            Tìm kiếm
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
            }}
          >
            Làm mới
          </Button>

          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
            Đóng
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{
            backgroundColor: "#ffc069",
            padding: 0,
          }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const columns = [
    {
      title: "Tên kho",
      dataIndex: "warehouseName",
      key: "warehouseName",
      ...getColumnSearchProps("warehouseName"),
    },
    {
      title: "Địa chỉ kho",
      dataIndex: "warehouseAddress",
      key: "warehouseAddress",
      ...getColumnSearchProps("warehouseAddress"),
    },
    {
      title: "Số lượng trong kho",
      dataIndex: "count",
      key: "count",
    },
  ];

  const handlePurchase = async () => {
    const customerName = document.querySelector(
      'input[name = "purchaseCustomerName"]'
    ).value;

    const formPurchase = document.querySelector('form[id = "purchase-form"]');

    if (formPurchase) {
      const body = {
        warehouseId,
        categoryId: form.categoryId,
        customerName,
      };
      let success = await $api.createPurchaseForm(body);
      if (success) {
        $toast.show({
          type: "success",
          message: `Bán thành công sản phẩm ${form.categoryId} cho khách hàng ${customerName}!`,
        });
        getWarehouses();
        formPurchase.reset();
      } else {
        $toast.show({
          type: "error",
          message: "Tạo giao dịch thất bại!",
        });
      }
    }
  };

  return (
    <div className={cx("distributor-wrap")}>
      <h1 className={cx("distributor__heading")}>Tạo giao dịch bán hàng</h1>
      <Form2 form={form} items={formItems} />

      <Table
        className={cx("distributor__table")}
        rowSelection={{
          type: selectionType,
          ...rowSelection,
        }}
        columns={columns}
        dataSource={warehouses}
        pagination={{ pageSize: 5 }}
        rowKey="id"
      ></Table>

      <div className={cx("purchase-form")}>
        <Form
          id="purchase-form"
          name="basic"
          layout="vertical"
          initialValues={{ remember: true }}
          autoComplete="off"
        >
          <div className={cx("purchase-form-flex")}>
            <Form.Item
              label={
                <p className={cx("distributor-form-label")}>Tên khách hàng</p>
              }
              name="purchaseCustomerName"
              rules={[
                { required: true, message: "Vui lòng nhập tên khách hàng!" },
              ]}
            >
              <Input
                className={cx("distributor-form-input")}
                placeholder="Nhập tên khách hàng..."
                name="purchaseCustomerName"
              />
            </Form.Item>

            <Form.Item>
              <button
                className={cx("purchase-submit-btn")}
                type="submit"
                onClick={handlePurchase}
              >
                Xác nhận bán hàng
              </button>
            </Form.Item>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default Purchase;
