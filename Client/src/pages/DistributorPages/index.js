import { DistributorLayout } from "@/layouts";
import { Route } from "react-router-dom";
import Home from "./Home";
import WareHouses from "./WareHouses";
import NewWarehouse from "./NewWarehouse";
import Orders from './Orders';
import Purchase from "./Purchase";
import ErrorProducts from "./ErrorProducts";
import Warranty from "./Warranty";
import NewWarrantyForm from "./NewWarrantyForm";
import DoneProducts from "./DoneProducts";

export default function DistributorPages() {
  return (
    <>
      <Route
        exact
        index
        element={
          <DistributorLayout>
            <Home />
          </DistributorLayout>
        }
      />
      <Route
        exact
        path="distribution-agent/warehouse"
        element={
          <DistributorLayout>
            <WareHouses />
          </DistributorLayout>
        }
      />
      <Route
        exact
        path="distribution-agent/warehouse/new"
        element={
          <DistributorLayout>
            <NewWarehouse />
          </DistributorLayout>
        }
      />
      <Route
        exact
        path="distribution-agent/orders"
        element = {
          <DistributorLayout>
            <Orders />
          </DistributorLayout>
        }
      />
      <Route
        exact
        path="distribution-agent/purchase"
        element = {
          <DistributorLayout>
            <Purchase />
          </DistributorLayout>
        }
      />
      <Route
        exact
        path="distribution-agent/done-products"
        element = {
          <DistributorLayout>
            <DoneProducts />
          </DistributorLayout>
        }
      />
      <Route
        exact
        path="distribution-agent/error-products"
        element = {
          <DistributorLayout>
            <ErrorProducts />
          </DistributorLayout>
        }
      />
      <Route
        exact
        path="distribution-agent/warranty"
        element = {
          <DistributorLayout>
            <Warranty />
          </DistributorLayout>
        }
      />
      <Route
        exact
        path="distribution-agent/warranty/new"
        element = {
          <DistributorLayout>
            <NewWarrantyForm />
          </DistributorLayout>
        }
      />

      
    </>
  );
}
