import { WarrantorLayout } from "@/layouts";
import { Route } from "react-router-dom";
import DashBoard from "./DashBoard";
import Import from "./Import";
import Progressing from "./Progressing";

const WarrantorPages = () => {
  return (
    <>
      <Route
        exact
        index
        element={
          <WarrantorLayout pageTitle="DashBoard">
            <DashBoard />
          </WarrantorLayout>
        }
      />
      <Route
        exact
        path="import"
        element={
          <WarrantorLayout pageTitle="Import">
            <Import />
          </WarrantorLayout>
        }
      />
      <Route
        exact
        path="progressing"
        element={
          <WarrantorLayout pageTitle="Progressing">
            <Progressing />
          </WarrantorLayout>
        }
      />
    </>
  );
};

export default WarrantorPages;
