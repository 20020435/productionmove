import Card from "@/components/Card";
import { useApi } from "@/hooks/useApi";
import { useLayoutEffect, useState } from "react";
import classNames from "classnames/bind";
import style from "./Progressing.module.scss";
import { useToast } from "@/hooks/useToast";
import { useLoading } from "@/hooks/useLoading";
import DataTable from "@/components/DataTable";

const cx = classNames.bind(style);

export default function Progressing() {
  const [history, setHistory] = useState([]);
  const $toast = useToast();
  const $loading = useLoading();
  const $api = useApi();

  const getInsuringProducts = async () => {
    const raw = await $api.getInsuringProducts();
    const data = [
      ...raw.map((item) => ({ ...item, category: item.category.name })),
    ];
    setHistory(data);
  };

  const resolveFixedProduct = async (id) => {
    $loading.start();
    let success = await $api.resolveFixedProduct(id);
    if (success) {
      getInsuringProducts();
      $toast.show({ type: "success", message: "Xác nhận sửa thành công!" });
    } else
      $toast.show({
        type: "error",
        message: "Có lỗi xảy ra!",
      });
    $loading.stop();
  };

  const rejectFixedProduct = async (id) => {
    $loading.start();
    let success = await $api.rejectFixedProduct(id);
    if (success) {
      getInsuringProducts();
      $toast.show({ type: "success", message: "Xác nhận trả về thành công!" });
    } else
      $toast.show({
        type: "error",
        message: "Có lỗi xảy ra!",
      });
    $loading.stop();
  };

  useLayoutEffect(() => {
    getInsuringProducts();
  }, []);

  const dataItems = [
    {
      value: "productId",
      heading: "ID Sản phẩm",
      flex: 3,
      align: "start",
    },
    {
      value: "code",
      heading: "Mã Sản phẩm",
      flex: 1,
      align: "start",
    },
    {
      value: "category",
      heading: "Thể loại",
      flex: 3,
      align: "start",
    },
    {
      value: "errorMessage",
      heading: "Ghi chú",
      flex: 1,
      align: "end",
    },
    {
      value: "actions",
      heading: "Actions",
      actions: [
        {
          text: "Sửa xong",
          bind: "id",
          color: "#00b300",
          click: async (bind) => {
            await resolveFixedProduct(bind);
          },
        },
        {
          text: "Trả về",
          bind: "id",
          color: "#ff3300",
          click: async (bind) => {
            await rejectFixedProduct(bind);
          },
        },
      ],
      flex: 5,
      align: "end",
    },
  ];

  return (
    <div className={cx("export")}>
      <Card width="100%">
        <h3>Danh sách sản phẩm lỗi đang chờ</h3>
        <DataTable data={history} dataItems={dataItems} />
      </Card>
    </div>
  );
}
