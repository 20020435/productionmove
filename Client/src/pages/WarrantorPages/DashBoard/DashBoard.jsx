import Card from "@/components/Card";
import DataChart from "@/components/DataChart";
import Form from "@/components/Form";
import { useApi } from "@/hooks/useApi";
import classNames from "classnames/bind";
import { useEffect, useState } from "react";
import style from "./DashBoard.module.scss";

const cx = classNames.bind(style);

export default function DashBoard() {
  const [dataBugRate, setDataBugRate] = useState([]);
  const $api = useApi();

  const getWarrantyProducts = async (statuses) => {
    const raw = await $api.getWarrantyProducts(statuses);
    const data = raw.map((item) => ({
      name: item.name,
      value: item.count,
    }));
    setDataBugRate(data);
  };

  useEffect(() => {
    getWarrantyProducts();
  }, []);

  const [bugForm] = useState({
    dateFrom: "",
    dateTo: "",
  });

  const bugItems = [
    {
      type: "pickDate",
      name: "dateFrom",
      title: "From",
      col: 4,
      rules: [(v) => (v !== "Invalid Date" ? "" : "Ngày không hợp lệ")],
      onchange: () => {
        const form = {};
        if (bugForm.dateFrom) form.dateFrom = bugForm.dateFrom;
        if (bugForm.dateTo) form.dateTo = bugForm.dateTo;
        if (bugForm.dateTo !== "Invalid Date") getWarrantyProducts(form);
      },
    },
    {
      type: "pickDate",
      name: "dateTo",
      title: "To",
      col: 4,
      rules: [(v) => (v !== "Invalid Date" ? "" : "Ngày không hợp lệ")],
      onchange: () => {
        const form = {};
        if (bugForm.dateFrom) form.dateFrom = bugForm.dateFrom;
        if (bugForm.dateTo) form.dateTo = bugForm.dateTo;
        if (bugForm.dateTo !== "Invalid Date") getWarrantyProducts(form);
      },
    },
  ];

  return (
    <div className={cx("dashboard")}>
      <Card width="100%">
        <div className={cx("heading")}>
          <h3>Thống kê các đơn bảo hành</h3>
          <div style={{ flex: 1 }}>
            <Form form={bugForm} items={bugItems} rlt />
          </div>
        </div>
        <DataChart
          height={500}
          data={dataBugRate}
          maxValue={10}
          nameField="Trạng thái"
          valueField="Số đơn"
        />
      </Card>
    </div>
  );
}
