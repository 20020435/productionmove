import classNames from "classnames/bind";
import style from "./NoPage.module.scss";

const cx = classNames.bind(style);

const NoPage = () => {
  return (
    <div className={cx("no-page")}>
      <h1>Page Not Found</h1>
    </div>
  );
};

export default NoPage;
