import React, { useState } from "react";
import classNames from "classnames/bind";
import style from "./NewProduct.module.scss";
import { Form, Input, message, Select, Cascader } from "antd";
import { emptyValidation } from "@/components/ValidationRule/validationRule";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useApi } from "@/hooks/useApi";
import { useToast } from "@/hooks/useToast";
import { useLoading } from "@/hooks/useLoading";

const cx = classNames.bind(style);

const NewProduct = () => {
  const { state } = useLocation();
  const [form] = useState(
    state || {
      warrantyPeriod: "",
      code: "",
      content: "",
      cylinderCapacity: "",
      engineType: "",
      fuelPumpSystem: "",
      gear: "",
      image: "",
      imageName: "",
      maxSpeed: "",
      maximumOutput: "",
      maximumTorque: "",
      name: "",
      price: "",
      shortDescription: "a",
      tankCapacity: "",
    }
  );
  console.log(form);
  const $navigate = useNavigate();
  const $api = useApi();
  const $toast = useToast();
  const $loading = useLoading();
  const { TextArea } = Input;

  const handleCreateProduct = async () => {
    $loading.start();
    let formData = new FormData();
    if (
      form.warrantyPeriod &&
      form.code &&
      form.content &&
      form.cylinderCapacity &&
      form.engineType &&
      form.fuelPumpSystem &&
      form.gear &&
      form.image &&
      form.maxSpeed &&
      form.maximumOutput &&
      form.maximumTorque &&
      form.name &&
      form.price &&
      form.shortDescription &&
      form.tankCapacity
    ) {
      formData.append("image", form.image, form.imageName);
      formData.append("name", form.name);
      formData.append("code", form.code);
      formData.append("price", form.price);
      formData.append("warrantyPeriod", form.warrantyPeriod);
      formData.append("shortDescription", form.shortDescription);
      formData.append("content", form.content);
      formData.append("engineType", form.engineType);
      formData.append("gear", form.gear);
      formData.append("cylinderCapacity", form.cylinderCapacity);
      formData.append("maximumTorque", form.maximumTorque);
      formData.append("maximumOutput", form.maximumOutput);
      formData.append("maxSpeed", form.maxSpeed);
      formData.append("tankCapacity", form.tankCapacity);
      formData.append("fuelPumpSystem", form.fuelPumpSystem);
      console.log(formData);
    }
    let success = await $api.createNewProduct(formData);
    if (success) {
      $toast.show({
        type: "success",
        message: `Thêm thành công sản phẩm ${form.name}!`,
      });
      $navigate("/admin/products");
    } else {
      $toast.show({
        type: "error",
        message: "Có lỗi xảy ra khi thêm sản phẩm!",
      });
    }
    $loading.stop();
  };

  const handleChange = (value, item) => {
    form[item] = value;
  };

  return (
    <div className={cx("manager-wrap")}>
      <h1 className={cx("manager__heading")}>
        {state ? "Sửa sản phẩm" : "Thêm sản phẩm"}
      </h1>
      <Link to={"/admin/products"}>
        <button className={cx("manager__btn", "manager__btn--left")}>
          Quay lại
        </button>
      </Link>
      <div className={cx("manager-form")}>
        <Form name="basic" layout="horizontal" autoComplete="off">
          <Form.Item
            label={<p className={cx("manager-form-label")}>Ảnh sản phẩm</p>}
            name="productImage"
            rules={[{ required: true, message: "Vui lòng nhập ảnh sản phẩm!" }]}
          >
            <Input
              className={cx("manager-form-input")}
              type="file"
              name="productImage"
              accept="image/*"
              onChange={(e) => {
                form.image = e.target.files[0];
                form.imageName = e.target.files[0].name;
              }}
            />
          </Form.Item>
          <Form.Item
            initialValue={form.name}
            label={<p className={cx("manager-form-label")}>Tên sản phẩm</p>}
            name="productName"
            rules={[{ required: true, message: "Vui lòng nhập tên sản phẩm!" }]}
          >
            <Input
              className={cx("manager-form-input")}
              type="text"
              placeholder="Nhập tên sản phẩm muốn thêm..."
              name="productName"
              onChange={(e) => handleChange(e.target.value, "name")}
            />
          </Form.Item>
          <Form.Item
            initialValue={form.code}
            label={<p className={cx("manager-form-label")}>Thể loại</p>}
            name="productCategory"
            rules={[{ required: true, message: "Vui lòng nhập mã sản phẩm!" }]}
          >
            <Input
              className={cx("manager-form-input")}
              type="text"
              placeholder="Nhập mã sản phẩm..."
              name="productCategory"
              onChange={(e) => handleChange(e.target.value, "code")}
            />
          </Form.Item>
          <Form.Item
            initialValue={form.price}
            label={<p className={cx("manager-form-label")}>Giá sản phẩm</p>}
            name="productPrice"
            rules={[{ required: true, message: "Vui lòng nhập giá sản phẩm!" }]}
          >
            <Input
              className={cx("manager-form-input")}
              type="text"
              placeholder="Nhập giá sản phẩm..."
              name="productPrice"
              onChange={(e) => handleChange(e.target.value, "price")}
            />
          </Form.Item>
          <Form.Item
            initialValue={form.warrantyPeriod}
            label={
              <p className={cx("manager-form-label")}>
                Thời gian bảo hành (tháng)
              </p>
            }
            name="productWarrantyPeriod"
            rules={[
              { required: true, message: "Vui lòng nhập thời gian bảo hành!" },
            ]}
          >
            <Input
              className={cx("manager-form-input")}
              type="number"
              placeholder="Nhập thời gian bảo hành (tháng)..."
              name="productWarrantyPeriod"
              onChange={(e) => handleChange(e.target.value, "warrantyPeriod")}
            />
          </Form.Item>

          <Form.Item
            initialValue={form.engineType}
            label={
              <p className={cx("manager-form-label", "manager__label-config")}>
                Thông tin cấu hình
              </p>
            }
          >
            <Form.Item
              initialValue={form.engineType}
              label={<p className={cx("manager-form-label")}>Kiểu động cơ</p>}
              name="productEngineType"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập kiểu động cơ!",
                },
              ]}
            >
              <Input
                className={cx("manager-form-input")}
                placeholder="Nhập kiểu động cơ"
                name="productEngineType"
                onChange={(e) => handleChange(e.target.value, "engineType")}
              />
            </Form.Item>

            <Form.Item
              initialValue={form.gear}
              label={<p className={cx("manager-form-label")}>Hộp số</p>}
              name="productGear"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập thông tin hộp số!",
                },
              ]}
            >
              <Input
                className={cx("manager-form-input")}
                placeholder="Nhập thông tin hộp số...)"
                name="productGear"
                onChange={(e) => handleChange(e.target.value, "gear")}
              />
            </Form.Item>

            <Form.Item
              initialValue={form.cylinderCapacity}
              label={
                <p className={cx("manager-form-label")}>
                  Dung tích xi lanh (cm³)
                </p>
              }
              name="productCylinderCapacity"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập dung tích xi lanh!",
                },
              ]}
            >
              <Input
                className={cx("manager-form-input")}
                placeholder="Nhập dung tích xi lanh..."
                name="productCylinderCapacity"
                onChange={(e) =>
                  handleChange(e.target.value, "cylinderCapacity")
                }
              />
            </Form.Item>

            <Form.Item
              initialValue={form.maximumOutput}
              label={
                <p className={cx("manager-form-label")}>
                  Công suất cực đại (Hp/rpm)
                </p>
              }
              name="productMaximumOutput"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập công suất cực đại!",
                },
              ]}
            >
              <Input
                className={cx("manager-form-input")}
                placeholder="Nhập công suất cực đại..."
                name="productMaximumOutput"
                onChange={(e) => handleChange(e.target.value, "maximumOutput")}
              />
            </Form.Item>

            <Form.Item
              initialValue={form.maximumTorque}
              label={
                <p className={cx("manager-form-label")}>
                  Mô-men xoắn cực đại (Nm/rpm)
                </p>
              }
              name="productMaximumTorque"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mô-men xoắn cực đại!",
                },
              ]}
            >
              <Input
                className={cx("manager-form-input")}
                placeholder="Nhập mô-men xoắn cực đại..."
                name="productMaximumTorque"
                onChange={(e) => handleChange(e.target.value, "maximumTorque")}
              />
            </Form.Item>

            <Form.Item
              initialValue={form.maxSpeed}
              label={
                <p className={cx("manager-form-label")}>Tốc độ tối đa (km/h)</p>
              }
              name="productMaxSpeed"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tốc độ tối đa!",
                },
              ]}
            >
              <Input
                className={cx("manager-form-input")}
                placeholder="Nhập tốc độ tối đa..."
                name="productMaxSpeed"
                onChange={(e) => handleChange(e.target.value, "maxSpeed")}
              />
            </Form.Item>

            <Form.Item
              initialValue={form.tankCapacity}
              label={
                <p className={cx("manager-form-label")}>
                  Dung tích thùng nhiên liệu (lít)
                </p>
              }
              name="productTankCapacity"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập dung tích thùng nl!",
                },
              ]}
            >
              <Input
                className={cx("manager-form-input")}
                placeholder="Nhập dung tích thùng nl..."
                name="productTankCapacity"
                onChange={(e) => handleChange(e.target.value, "tankCapacity")}
              />
            </Form.Item>

            <Form.Item
              initialValue={form.fuelPumpSystem}
              label={
                <p className={cx("manager-form-label")}>
                  Hệ thống bơm nhiên liệu
                </p>
              }
              name="productFuelPumpSystem"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập hệ thống bơm nl!",
                },
              ]}
            >
              <Input
                className={cx("manager-form-input")}
                placeholder="Nhậphệ thống bơm nl..."
                name="productFuelPumpSystem"
                onChange={(e) => handleChange(e.target.value, "fuelPumpSystem")}
              />
            </Form.Item>
          </Form.Item>

          <Form.Item
            initialValue={form.content}
            label={<p className={cx("manager-form-label")}>Mô tả chi tiết</p>}
            name="productDescription"
          >
            <TextArea
              className={cx("manager-form-input")}
              placeholder="Nhập mô tả chi tiết sản phẩm"
              name="productDescription"
              onChange={(e) => handleChange(e.target.value, "content")}
            />
          </Form.Item>

          <Form.Item>
            <button
              className={cx("manager-submit-btn")}
              onClick={handleCreateProduct}
            >
              {state ? "Sửa sản phẩm" : "Thêm sản phẩm"}
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default NewProduct;
