import Card from "@/components/Card";
import Form from "@/components/Form";
import { useApi } from "@/hooks/useApi";
import {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useLayoutEffect,
  useRef,
  useState,
} from "react";
import { useSelector } from "react-redux";
import classNames from "classnames/bind";
import style from "./Export.module.scss";
import { useToast } from "@/hooks/useToast";
import { useLoading } from "@/hooks/useLoading";
import DataTable from "@/components/DataTable";
import Button from "@/components/Button";

const cx = classNames.bind(style);

const ShowQuantity = forwardRef((props, ref) => {
  const [quantity, setQuantity] = useState(0);

  useImperativeHandle(ref, () => ({
    emit(value) {
      if (value) setQuantity(value);
      else setQuantity(0);
    },
  }));

  return (
    <div className={cx("ShowQuantity")}>Lượng hàng trong kho: {quantity}</div>
  );
});

export default function Export() {
  const [history, setHistory] = useState([]);
  const quantityCategory = useSelector((state) => {
    const quantities = state.data.quantityCategory;
    const res = {};
    if (quantities)
      quantities.forEach((quantity) => {
        res[quantity.category.id] = quantity.count;
      });
    return res;
  });
  const [form, setForm] = useState({
    categoryId: "",
    count: 0,
    distributionAgentId: "",
  });
  const $toast = useToast();
  const $loading = useLoading();
  const formElement = useRef(null);
  const showQuantity = useRef(null);
  const $api = useApi();

  const getHistory = async () => {
    const raw = await $api.getExportHistory();
    const data = [
      ...raw.map((item) => ({
        ...item,
        category: item.category.name,
        image: item.category.image,
      })),
    ];
    setHistory(data);
  };

  useLayoutEffect(() => {
    $api.getAllCategory();
    $api.getAllDistribution();
    $api.getQuantityByCategory();
    getHistory();
  }, []);

  useEffect(() => {
    showQuantity.current.emit(quantityCategory[form.categoryId]);
  });

  const formItems = [
    {
      type: "dropList",
      name: "categoryId",
      title: "Thể loại",
      options: "categories",
      textOptions: "name",
      valueOptions: "id",
      rules: [
        (v) => (v ? "" : "Vui lòng nhập Thể loại"),
        (v) => (v !== "error" ? "" : "Thể loại không hợp lệ"),
      ],
      col: 4,
      onchange: () => {
        showQuantity.current.emit(quantityCategory[form.categoryId]);
      },
    },
    {
      type: "numberField",
      name: "count",
      title: "Số lượng",
      rules: [
        (v) => (v ? "" : "Vui lòng nhập Số lượng"),
        (v) => (/^\d+$/.test(v) ? "" : "Vui lòng nhập số tự nhiên"),
      ],
      col: 4,
    },
    {
      type: "dropList",
      name: "distributionAgentId",
      title: "Distribution",
      options: "distributions",
      textOptions: "name",
      valueOptions: "id",
      rules: [
        (v) => (v ? "" : "Vui lòng nhập Distribution"),
        (v) => (v !== "error" ? "" : "Distribution không hợp lệ"),
      ],
      col: 4,
    },
  ];

  const dataItems = [
    {
      value: "distributionAgent",
      heading: "Đại lí nhận lô hàng",
      flex: 3,
      align: "start",
    },
    {
      value: "category",
      heading: "Thể loại",
      flex: 1,
      align: "start",
    },
    {
      value: "image",
      heading: "Ảnh",
      flex: 3,
      align: "start",
      format: (value) => <img style={{ height: 50 }} alt="image" src={value} />,
    },
    {
      value: "count",
      heading: "Số lượng",
      flex: 1,
      align: "end",
    },
    {
      value: "created",
      heading: "Ngày xuất",
      flex: 3,
      align: "end",
    },
  ];

  const handleExport = async () => {
    if (form.count > quantityCategory[form.categoryId])
      $toast.show({
        type: "error",
        message: `Lượng hàng không đủ: ${quantityCategory[form.categoryId]}`,
      });
    else
      try {
        $loading.start();
        let success = await $api.exportProducts(form);
        if (success) {
          $toast.show({
            type: "success",
            message: `Xuất hàng thành công! Thể loại: ${form.categoryId}, Số lượng: ${form.count}, Distribution ID: ${form.categoryId}`,
          });
          getHistory();
        } else $toast.show({ type: "error", message: `Có lỗi xảy ra` });
        $loading.stop();
      } catch (error) {
        $toast.show({ type: "error", message: `Có lỗi xảy ra` });
        console.log(error);
      }
    setForm({
      categoryId: "",
      count: 0,
      distributionAgentId: "",
    });
  };

  return (
    <div className={cx("export")}>
      <Card width="100%">
        <div className={cx("heading")}>
          <h3>Xuất lô hàng</h3>
          <Button
            text="Create"
            onclick={() => formElement.current.submit(handleExport)}
            color="#5d7b6f"
            outlined
            upCase
          />
        </div>
        <Form ref={formElement} form={form} items={formItems} />
        <ShowQuantity ref={showQuantity} />
      </Card>
      <Card width="100%">
        <h3>Lịch sử xuất lô</h3>
        <DataTable data={history} dataItems={dataItems} />
      </Card>
    </div>
  );
}
