import Card from "@/components/Card";
import { useApi } from "@/hooks/useApi";
import { useEffect, useState } from "react";
import classNames from "classnames/bind";
import style from "./Insurance.module.scss";
import { useToast } from "@/hooks/useToast";
import { useLoading } from "@/hooks/useLoading";
import DataTable from "@/components/DataTable";

const cx = classNames.bind(style);

export default function Insurance() {
  const [history, setHistory] = useState([]);
  const $toast = useToast();
  const $loading = useLoading();
  const $api = useApi();

  const getBugProducts = async () => {
    const raw = await $api.getBugProducts();
    const data = [
      ...raw.map((item) => ({ ...item, category: item.category.name })),
    ];
    setHistory(data);
  };

  useEffect(() => {
    getBugProducts();
  }, []);

  const dataItems = [
    {
      value: "productId",
      heading: "ID Sản phẩm",
      flex: 3,
      align: "start",
    },
    {
      value: "code",
      heading: "Mã Sản phẩm",
      flex: 1,
      align: "start",
    },
    {
      value: "category",
      heading: "Thể loại",
      flex: 3,
      align: "start",
    },
    {
      value: "errorMessage",
      heading: "Ghi chú",
      flex: 1,
      align: "end",
    },
    {
      value: "actions",
      heading: "Actions",
      actions: [
        {
          text: "Xác nhận",
          bind: "id",
          click: async (bind) => {
            $loading.start();
            let success = await $api.acceptErrorProduct(bind);
            if (success) {
              $toast.show({ type: "success", message: "Đã xác nhận!" });
              getBugProducts();
            } else $toast.show({ type: "error", message: "Có lỗi xảy ra!" });
            $loading.stop();
          },
        },
      ],
      flex: 3,
      align: "end",
    },
  ];

  return (
    <div className={cx("export")}>
      <Card width="100%">
        <h3>Danh sách sản phẩm lỗi đang chờ</h3>
        <DataTable data={history} dataItems={dataItems} />
      </Card>
    </div>
  );
}
