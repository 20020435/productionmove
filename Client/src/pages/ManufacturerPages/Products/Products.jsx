import Card from "@/components/Card";
import Form from "@/components/Form";
import { useApi } from "@/hooks/useApi";
import { useLayoutEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import classNames from "classnames/bind";
import style from "./Products.module.scss";
import { useToast } from "@/hooks/useToast";
import DataTable from "@/components/DataTable";
import { useLoading } from "@/hooks/useLoading";
import Button from "@/components/Button";

const cx = classNames.bind(style);

export default function Products() {
  const [history, setHistory] = useState([]);
  const [form, setForm] = useState({
    categoryId: "",
    count: 0,
  });
  const $toast = useToast();
  const $isLoading = useSelector((state) => state.loading.value);
  const $loading = useLoading();
  const formElement = useRef(null);
  const $api = useApi();

  const getHistory = async () => {
    const raw = await $api.getProductsHistory();
    const data = [
      ...raw.map((item) => ({
        ...item,
        category: item.category.name,
        image: item.category.image,
      })),
    ];
    setHistory(data);
  };

  useLayoutEffect(() => {
    $api.getAllCategory();
    getHistory();
  }, []);

  const formItems = [
    {
      type: "dropList",
      name: "categoryId",
      title: "Thể loại",
      options: "categories",
      textOptions: "name",
      valueOptions: "id",
      rules: [
        (v) => (v ? "" : "Vui lòng nhập Thể loại"),
        (v) => (v !== "error" ? "" : "Thể loại không hợp lệ"),
      ],
      col: 6,
    },
    {
      type: "numberField",
      name: "count",
      title: "Số lượng",
      rules: [
        (v) => (v ? "" : "Vui lòng nhập Số lượng"),
        (v) => (/^\d+$/.test(v) ? "" : "Vui lòng nhập số tự nhiên"),
      ],
      col: 6,
    },
  ];

  const dataItems = [
    {
      value: "category",
      heading: "Thể loại",
      flex: 1,
      align: "start",
    },
    {
      value: "image",
      heading: "Ảnh",
      flex: 3,
      align: "start",
      format: (value) => <img style={{ height: 50 }} alt="image" src={value} />,
    },
    {
      value: "count",
      heading: "Số lượng",
      flex: 2,
      align: "start",
    },
    {
      value: "created",
      heading: "Ngày xuất",
      flex: 3,
      align: "end",
    },
  ];

  const handleCreate = async () => {
    try {
      $loading.start();
      let success = await $api.createProducts(form);
      if (success) {
        $toast.show({
          type: "success",
          message: `Tạo thành công! Thể loại: ${form.categoryId}, Số lượng: ${form.count}`,
        });
        getHistory();
      } else $toast.show({ type: "error", message: `Có lỗi xảy ra` });
      $loading.stop();
    } catch (error) {
      $toast.show({ type: "error", message: `Có lỗi xảy ra` });
      console.log(error);
    }
    setForm({
      categoryId: "",
      count: 0,
    });
  };

  return (
    <div className={cx("products")}>
      <Card width="100%">
        <div className={cx("heading")}>
          <h3>Nhập lô sản phẩm mới</h3>
          <Button
            text="Create"
            onclick={() => formElement.current.submit(handleCreate)}
            color="#5d7b6f"
            outlined
            upCase
          />
        </div>
        <Form ref={formElement} form={form} items={formItems} />
      </Card>
      <Card width="100%">
        <h3>Lịch sử nhập lô</h3>
        <DataTable data={history} dataItems={dataItems} />
      </Card>
    </div>
  );
}
