export function getLocalRefreshToken() {
  const refreshToken = JSON.parse(localStorage.getItem("refreshToken"));
  if (refreshToken) {
    if (new Date(refreshToken.expires) < new Date()) {
      localStorage.removeItem("refreshToken");
      return null;
    }
    return refreshToken.token;
  }
  return null;
}
export function getLocalAccessToken() {
  const accessToken = JSON.parse(localStorage.getItem("accessToken"));
  if (accessToken) {
    if (new Date(accessToken.expires) < new Date()) {
      localStorage.removeItem("accessToken");
      return null;
    }
    return accessToken.token;
  }
  return null;
}
export function setLocalRefreshToken(refreshToken) {
  localStorage.setItem("refreshToken", refreshToken);
}
export function setLocalAccessToken(accessToken) {
  localStorage.setItem("accessToken", accessToken);
}
export function removeLocalToken() {
  localStorage.removeItem("refreshToken");
  localStorage.removeItem("accessToken");
}
